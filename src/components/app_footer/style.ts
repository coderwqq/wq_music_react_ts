import styled from 'styled-components'

export const FooterWrapper = styled.div`
  margin-top: -1px;
  height: 325px;
  font-size: 12px;
  border-top: 1px solid #d3d3d3;

  > .inner {
    width: 980px;
    margin: 0 auto;

    .list {
      display: flex;
      margin-top: 33px;
      color: rgba(0, 0, 0, 0.5);

      .item {
        width: 45px;
        margin-left: 80px;

        .text {
          display: block;
          width: 100px;
          text-align: center;
          margin-left: -27.5px;
          margin-top: 10px;
        }
      }
    }
  }
`
interface IIcon {
  itemIcon: any
}
export const IconWrapper = styled.a<IIcon>`
  display: inline-block;
  width: 45px;
  height: 45px;
  background-size: 220px 220px;
  background-position: ${(props) => props.itemIcon.background};

  &:hover {
    background-position: ${(props) => props.itemIcon.bg_hover};
  }
`
export const CopyWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 60px;
  color: #666;

  a {
    color: #666;
    &:hover {
      text-decoration: underline;
    }
  }

  > ul {
    display: flex;
    margin-bottom: 6px;

    li {
      .line {
        margin: 0 8px;
        color: #d9d9d9;
      }
      &:last-child {
        .line {
          display: none;
        }
      }
    }
  }
  > p {
    margin: 6px 0;

    .report,
    .email,
    .owner {
      margin-right: 14px;
    }
    .span {
      margin-right: 5px;
    }
    .police-logo {
      display: inline-block;
      width: 14px;
      height: 14px;
      margin: 0 2px 0 10px;
      vertical-align: -2px;
      background: url(${require('@/assets/img/police.png')}) no-repeat;
      background-size: 14px;
    }
  }
`
