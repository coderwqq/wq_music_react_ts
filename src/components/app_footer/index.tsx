import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { FooterWrapper, IconWrapper, CopyWrapper } from './style'
import { footerIcon } from '@/assets/data/footer_icon'
import { footerLinks } from '@/assets/data/local_data'

interface IProps {
  children?: ReactNode
}

const AppFooter: FC<IProps> = () => {
  return (
    <FooterWrapper>
      <div className="inner">
        <ul className="list">
          {footerIcon.map((item) => {
            return (
              <li key={item.text} className="item">
                <IconWrapper
                  className="icon sprite_footer_03"
                  itemIcon={item}
                  href={item.link}
                  target="_blank"
                ></IconWrapper>
                <span className="text">{item.text}</span>
              </li>
            )
          })}
        </ul>
        <CopyWrapper className="copy">
          <ul>
            {footerLinks.map((item) => {
              return (
                <li key={item.title}>
                  <a href={item.link} rel="noreferrer" target="_blank">
                    {item.title}
                  </a>
                  <span className="line">|</span>
                </li>
              )
            })}
          </ul>
          <p>
            <a
              href="https://jubao.163.com/"
              rel="noreferrer"
              target="_blank"
              className="report"
            >
              廉政举报
            </a>
            <span className="email">
              不良信息举报邮箱: 51jubao@service.netease.com
            </span>
            <span>客服热线: 95163298</span>
          </p>
          <p>
            <span className="span">
              互联网宗教信息服务许可证: 浙 (2022) 0000120
            </span>
            <span className="span">增值电信业务经营许可证: 浙B2-20150198</span>
            <a
              href="https://beian.miit.gov.cn/#/Integrated/index"
              rel="noreferrer"
              target="_blank"
            >
              粤B2-20090191-18 工业和信息化部备案管理系统网站
            </a>
          </p>
          <p>
            <span className="owner">网易公司版权所有©1997-2023</span>
            <span>杭州乐读科技有限公司运营：</span>
            <a
              href="https://p5.music.126.net/obj/wo3DlcOGw6DClTvDisK1/24498695788/9de7/9e78/fc8d/35d33543c69c9f4c5ac8aeb937217597.png"
              rel="noreferrer"
              target="_blank"
            >
              浙网文[2021] 1186-054号
            </a>
            <a
              href="https://www.beian.gov.cn/portal/registerSystemInfo?recordcode=33010902002564"
              rel="noreferrer"
              target="_blank"
            >
              <span className="police-logo"></span>
              <span>浙公网安备 33010902002564号</span>
            </a>
          </p>
        </CopyWrapper>
      </div>
    </FooterWrapper>
  )
}

export default memo(AppFooter)
