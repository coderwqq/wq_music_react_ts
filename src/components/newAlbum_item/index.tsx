import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { AlbumItemWrapper } from './style'
import { getImageSize } from '@/utils/format'

interface IProps {
  children?: ReactNode
  itemData: any
}

const NewAlbumItem: FC<IProps> = (props) => {
  const { itemData } = props

  const artist = itemData.artists.map((item: any) => {
    return item.name
  })

  return (
    <AlbumItemWrapper>
      <div className="album-top">
        <img src={getImageSize(itemData.picUrl, 100)} alt="" />
        <i className="album-cover sprite_cover"></i>
        <i className="album-icon sprite_icon"></i>
      </div>
      <div className="album-info">
        <div className="album-name">{itemData.name}</div>
        <div className="artist">{artist}</div>
      </div>
    </AlbumItemWrapper>
  )
}

export default memo(NewAlbumItem)
