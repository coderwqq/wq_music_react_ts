import styled from 'styled-components'

export const AlbumItemWrapper = styled.div`
  width: 118px;
  margin-left: 11px;

  .album-top {
    position: relative;
    margin-bottom: 7px;

    .album-cover {
      position: absolute;
      left: 0;
      top: 0;
      width: 118px;
      height: 100px;
      background-position: 0 -570px;
      cursor: pointer;
    }
    .album-icon {
      display: none;
      position: absolute;
      right: 23px;
      bottom: 5px;
      width: 22px;
      height: 22px;
      background-position: 0 -85px;
      cursor: pointer;

      &:hover {
        background-position: 0 -110px;
      }
    }
    &:hover {
      .album-icon {
        display: inline-block;
      }
    }
  }
  .album-info {
    width: 90%;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;

    .album-name {
      font-size: 13px;
      color: #000;

      &:hover {
        text-decoration: underline;
        cursor: pointer;
      }
    }
    .artist {
      font-size: 12px;
      color: #666;

      &:hover {
        text-decoration: underline;
        cursor: pointer;
      }
    }
  }
`
