import styled from 'styled-components'

export const PagingWrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 20px 0;

  .ant-pagination {
    .ant-pagination-options {
      display: none;
    }
  }
`
