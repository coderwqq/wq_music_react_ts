import React, { memo, FC, useState } from 'react'
import type { ReactNode } from 'react'
import type { PaginationProps } from 'antd'
import { Pagination } from 'antd'
import { shallowEqual } from 'react-redux'

import { useAppDispatch, useAppSelector } from '@/store'
import { PagingWrapper } from './style'
import { fetchSongsMenuDataAction } from '@/store/modules/discover/songsMenu'

interface IProps {
  children?: ReactNode
  itemData?: any
}

const Paging: FC<IProps> = (props) => {
  const { itemData } = props
  const [currentPage, setCurrentPage] = useState(1)
  const { categorySongs } = useAppSelector(
    (state) => ({
      categorySongs: state.songsMenuReducer.categorySongs
    }),
    shallowEqual
  )

  const dispatch = useAppDispatch()
  const onChange: PaginationProps['onChange'] = (page) => {
    setCurrentPage(page)
    dispatch(fetchSongsMenuDataAction(page))

    window.scroll(0, 0)
  }

  return (
    <PagingWrapper>
      <Pagination
        current={currentPage}
        onChange={onChange}
        total={categorySongs.total}
        pageSize={35}
      />
    </PagingWrapper>
  )
}

export default memo(Paging)
