import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'
import { NavLink } from 'react-router-dom'
import { Input } from 'antd'
import { SearchOutlined } from '@ant-design/icons'

import { HeaderWrapper, LeftWrapper, RightWrapper } from './style'
import headerTitle from '@/assets/data/header_title.json'

interface IProps {
  children?: ReactNode
}

function showItem(item: any) {
  if (item.type === 'path') {
    return (
      <NavLink to={item.link}>
        {item.title}
        <i className="icon"></i>
      </NavLink>
    )
  } else {
    return (
      <a href={item.link} rel="noreferrer" target="_blank">
        {item.title}
      </a>
    )
  }
}

const AppHeader: FC<IProps> = () => {
  return (
    <HeaderWrapper>
      <div className="wrapV1">
        <LeftWrapper>
          <a className="logo sprite_01">网易云音乐</a>
          <div className="title-list">
            {headerTitle.map((item) => {
              return (
                <div className="item" key={item.title}>
                  {showItem(item)}
                </div>
              )
            })}
          </div>
        </LeftWrapper>
        <RightWrapper>
          <Input
            placeholder="音乐/视频/电台/用户"
            prefix={<SearchOutlined />}
            className="search"
          ></Input>
          <a
            href="https://music.163.com/login?targetUrl=%2Fcreatorcenter"
            rel="noreferrer"
            target="_blank"
            className="framer"
          >
            创作者中心
          </a>
          <a className="login">登录</a>
        </RightWrapper>
      </div>
      <div className="divide"></div>
    </HeaderWrapper>
  )
}

export default memo(AppHeader)
