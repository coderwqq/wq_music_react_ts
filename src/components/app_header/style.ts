import styled from 'styled-components'

export const HeaderWrapper = styled.div`
  height: 75px;
  color: #fff;
  background: #242424;

  .wrapV1 {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  .divide {
    height: 5px;
    background-color: ${(props) => props.theme.color.primary};
  }
`
export const LeftWrapper = styled.div`
  display: flex;

  .logo {
    display: block;
    width: 177px;
    height: 70px;
    background-position: 0 0;
    text-indent: -9999px;
  }
  .title-list {
    display: flex;
    align-items: center;

    .item {
      position: relative;

      a {
        display: block;
        padding: 0 20px;
        color: #ccc;
        line-height: 70px;
        cursor: pointer;
      }
      &:hover a,
      .active {
        color: #fff;
        background-color: #000;
      }
      .active .icon {
        position: absolute;
        background-image: url(${require('@/assets/img/sprite_01.png')});
        background-position: -226px 0;
        left: 50%;
        top: 64px;
        width: 12px;
        height: 7px;
        margin-left: -6px;
      }
    }
    :last-of-type {
      position: relative;

      ::after {
        position: absolute;
        content: '';
        width: 28px;
        height: 19px;
        background-image: url(${require('@/assets/img/sprite_01.png')});
        background-position: -190px 0;
        top: 20px;
        right: -19px;
      }
    }
  }
`
export const RightWrapper = styled.div`
  display: flex;
  align-items: center;
  font-size: 12px;

  .search {
    width: 158px;
    height: 32px;
    border-radius: 16px;

    input {
      &::placeholder {
        font-size: 12px;
      }
    }
  }
  .framer {
    box-sizing: border-box;
    width: 90px;
    height: 32px;
    line-height: 30px;
    margin-left: 12px;
    padding-left: 14px;
    color: #ccc;
    border: 1px solid #4f4f4f;
    border-radius: 16px;
    cursor: pointer;

    &:hover {
      color: #fff;
      border-color: #ccc;
    }
  }
  .login {
    margin: 0 22px 0 20px;
    color: #787878;
    cursor: pointer;

    &:hover {
      text-decoration: underline;
    }
  }
`
