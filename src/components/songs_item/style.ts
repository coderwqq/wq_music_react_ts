import styled from 'styled-components'

export const SongsItemWrapper = styled.div`
  width: 140px;
  padding-bottom: 30px;

  .info {
    .top {
      position: relative;

      .cover {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-position: 0 0;
      }
      .bottom {
        box-sizing: border-box;
        position: absolute;
        display: flex;
        justify-content: space-between;
        align-items: center;
        left: 0;
        bottom: 2px;
        width: 100%;
        height: 27px;
        padding: 0 10px;
        background-position: 0 -537px;

        .headset {
          display: inline-block;
          width: 14px;
          height: 11px;
          margin: 0 5px -1px 0;
          background-position: 0 -24px;
        }
        .count {
          color: #ccc;
          font-size: 12px;
        }
        .play {
          display: inline-block;
          width: 16px;
          height: 17px;
          background-position: 0 0;
          cursor: pointer;
        }
      }
    }
  }
  .down {
    margin: 8px 0 3px;

    .radio {
      display: inline-block;
      width: 35px;
      height: 15px;
      margin: 0 3px -1px 0;
      background-position: -31px -658px;
    }
    .name {
      line-height: 20px;
      cursor: pointer;
    }
    &:hover {
      .name {
        text-decoration: underline;
      }
    }
  }
`
