import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { SongsItemWrapper } from './style'
import { formatCount, getImageSize } from '@/utils/format'

interface IProps {
  children?: ReactNode
  itemData: any
}

const SongsItem: FC<IProps> = (props) => {
  const { itemData } = props

  return (
    <SongsItemWrapper>
      <div className="info">
        <div className="top">
          <img src={getImageSize(itemData.picUrl, 140)} alt="" />
          <div className="cover sprite_cover"></div>
          <div className="bottom sprite_cover">
            <span>
              <i className="headset sprite_icon"></i>
              <span className="count">{formatCount(itemData.playCount)}</span>
            </span>
            <i className="play sprite_icon"></i>
          </div>
        </div>
      </div>
      <div className="down">
        {itemData.highQuality && <i className="radio sprite_icon2"></i>}
        <a className="name">{itemData.name}</a>
      </div>
    </SongsItemWrapper>
  )
}

export default memo(SongsItem)
