import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'
import { Link } from 'react-router-dom'

import { SectionHeaderWrapper } from './style'

interface IProps {
  children?: ReactNode
  title: string
  keyWords?: string[]
  moreText?: string
  moreLink?: string
}

const SectionHeader: FC<IProps> = (props) => {
  const { title, keyWords, moreText, moreLink = '/' } = props

  return (
    <SectionHeaderWrapper>
      <div className="header-left">
        <i className="icon sprite_02"></i>
        <span className="title">{title}</span>
        <div className="keyWords">
          {keyWords?.map((item) => {
            return (
              <div key={item} className="key-list">
                <span className="item">{item}</span>
                <span className="line">|</span>
              </div>
            )
          })}
        </div>
      </div>
      <div className="header-right">
        <Link className="more" to={moreLink}>
          {moreText}
        </Link>
        <i className="arrow sprite_02"></i>
      </div>
    </SectionHeaderWrapper>
  )
}

export default memo(SectionHeader)
