import styled from 'styled-components'

export const SectionHeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: 12px;
  height: 33px;
  color: #666;
  border-bottom: 2px solid #c10d0c;

  .header-left {
    display: flex;
    align-items: center;

    .icon {
      width: 30px;
      height: 30px;
      margin-right: 4px;
      background-position: -225px -156px;
    }
    .title {
      font-size: 20px;
      color: #333;
      cursor: pointer;
    }
    .keyWords {
      display: flex;
      margin-left: 20px;
      cursor: pointer;

      .key-list {
        .item:hover {
          text-decoration: underline;
        }
        .line {
          margin: 0 10px;
          color: #ccc;
        }
        &:last-child {
          .line {
            display: none;
          }
        }
      }
    }
  }
  .header-right {
    padding-right: 10px;

    .more {
      &:hover {
        text-decoration: underline;
        cursor: pointer;
      }
    }
    .arrow {
      display: inline-block;
      width: 12px;
      height: 12px;
      background-position: 0 -240px;
      margin-left: 4px;
    }
  }
`
