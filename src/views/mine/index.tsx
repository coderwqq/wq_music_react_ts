import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { MineWrapper } from './style'

interface IProps {
  children?: ReactNode
}

const Mine: FC<IProps> = () => {
  return (
    <MineWrapper>
      <div className="mine_sprite mine">
        <a className="mine_sprite btn">立即登录</a>
      </div>
    </MineWrapper>
  )
}

export default memo(Mine)
