import styled from 'styled-components'

export const MineWrapper = styled.div`
  margin: 0 auto;
  width: 980px;
  min-height: 700px;
  background-color: #fff;
  border: 1px solid #d3d3d3;

  .mine {
    margin: 0 auto;
    width: 807px;
    height: 268px;
    padding-top: 104px;
    background-position: 0 104px;

    .btn {
      display: inline-block;
      width: 167px;
      height: 45px;
      margin: 202px 0 0 482px;
      background-position: 0 -278px;
      text-indent: -9999px;
    }
  }
`
