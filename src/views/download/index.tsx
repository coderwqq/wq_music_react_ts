import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { DownloadWrapper } from './style'
import { downloadData } from '@/assets/data/download_data'
import Client from './c-views/client'
import IntroduceV1 from './c-views/introduce_v1'
import IntroduceV2 from './c-views/introduce_v2'

interface IProps {
  children?: ReactNode
}

const Download: FC<IProps> = () => {
  return (
    <DownloadWrapper>
      <div className="QRcode">
        <img
          src="https://p1.music.126.net/wCRnpjXHpGRiq1c1uCkIFQ==/109951164246717547.png"
          alt=""
        />
        <p className="text">扫描二维码下载</p>
      </div>
      <Client />
      {downloadData.map((item) => {
        return (
          <div key={item.id}>
            {Number(item.id) % 2 !== 0 && <IntroduceV1 itemData={item} />}
            {Number(item.id) % 2 === 0 && <IntroduceV2 itemData={item} />}
          </div>
        )
      })}
    </DownloadWrapper>
  )
}

export default memo(Download)
