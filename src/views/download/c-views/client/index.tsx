import React, { memo, FC, useState } from 'react'
import type { ReactNode } from 'react'

import { ClientWrapper, LeftWrapper, RightWrapper } from './style'
import ShowQRcode from '../showPanel/show_QRcode'
import OtherClient from '../showPanel/other_client'

interface IProps {
  children?: ReactNode
}

const Client: FC<IProps> = () => {
  const [showQRcode, setShowQRcode] = useState(false)
  const [showOtherClient, setShowOtherClient] = useState(false)

  function showPanelClick(data: any) {
    if (data === 111) {
      setShowQRcode(false)
      setShowOtherClient(!showOtherClient)
    } else if (data === 222) {
      setShowOtherClient(false)
      setShowQRcode(!showQRcode)
    }
  }

  return (
    <ClientWrapper>
      <div className="inner">
        <div className="client">
          <LeftWrapper>
            <div className="title">在电脑上听</div>
            <img
              src="https://p1.music.126.net/PGNd0EAtUgA7iRCWXPnUuA==/109951164207162781.png?param=800x0"
              alt=""
            />
            <div className="windows">
              <span>
                <i className="icon_mac_iphone mac_iphone"></i>
              </span>
              <span>
                <i className="icon_windows pc"></i>
              </span>
            </div>
            <button className="computer">下载电脑端</button>
          </LeftWrapper>
          <RightWrapper>
            <div className="title">在手机上听</div>
            <img
              src="https://p1.music.126.net/sLtya87wVHWn-HWJ33oN4g==/109951164207180884.png?param=450x0"
              alt=""
            />
            <div className="phone">
              <span>
                <i className="icon_iphone iphone"></i>
              </span>
              <span>
                <i className="icon_android android"></i>
              </span>
            </div>
            <div className="mobile">
              <button
                className="mobile-download"
                onClick={() => showPanelClick(222)}
              >
                <i className="icon_ncode ncode"></i>
                下载手机端
              </button>
              {showQRcode && <ShowQRcode />}
            </div>
          </RightWrapper>
        </div>
        <div className="others">
          <div className="top" onClick={() => showPanelClick(111)}>
            <i className="download_arrow arrow"></i>
            其他操作系统客户端
          </div>
          {showOtherClient && <OtherClient />}
        </div>
      </div>
    </ClientWrapper>
  )
}

export default memo(Client)
