import styled from 'styled-components'

export const ClientWrapper = styled.div`
  margin: 0 auto;
  width: 100%;
  height: 100%;
  background-color: rgb(22, 22, 22);

  .inner {
    position: relative;
    margin: 0 auto;
    width: 1100px;
    color: #fff;

    .client {
      display: flex;
      justify-content: space-between;
      padding: 80px 20px 112px 0;
    }
    .others {
      position: absolute;
      top: 0;
      right: 0;

      .top {
        line-height: 60px;
        cursor: pointer;
        &:hover {
          opacity: 0.7;
        }
      }

      .arrow {
        display: inline-block;
        width: 20px;
        height: 20px;
        margin-right: 6px;
        background-size: contain;
        vertical-align: text-bottom;
      }
    }
  }
`
export const LeftWrapper = styled.div`
  margin: 0 auto;

  .title {
    font-size: 22px;
    margin-bottom: 23px;
    text-align: center;
  }
  img {
    display: block;
    width: 464px;
    height: 273px;
    margin: 0 auto 3px;
  }
  .windows {
    margin: 26px auto;
    text-align: center;

    .mac_iphone {
      display: inline-block;
      width: 154px;
      height: 44px;
      background-size: contain;
    }
    .pc {
      display: inline-block;
      width: 144px;
      height: 44px;
      margin-left: 40px;
      background-size: contain;
    }
  }
  .computer {
    display: block;
    margin: -10px auto 0;
    width: 300px;
    height: 65px;
    font-size: 22px;
    color: #d10000;
    border-radius: 32.5px;
    background-color: #fff;

    &:hover {
      opacity: 0.7;
      cursor: pointer;
    }
  }
`
export const RightWrapper = styled.div`
  margin-left: 220px;
  width: 300px;

  .title {
    font-size: 22px;
    margin-bottom: 23px;
    text-align: center;
  }
  img {
    display: block;
    width: 246px;
    height: 273px;
    margin: 0 auto 3px;
  }
  .phone {
    margin: 26px auto;
    text-align: center;

    .iphone,
    .android {
      display: inline-block;
      width: 120px;
      height: 44px;
      background-size: contain;
    }
    .android {
      margin-left: 40px;
    }
  }
  .mobile {
    position: relative;

    .mobile-download {
      display: block;
      margin: -10px auto 0;
      width: 300px;
      height: 65px;
      font-size: 22px;
      color: #d10000;
      border-radius: 32.5px;
      background-color: #fff;

      &:hover {
        opacity: 0.7;
        cursor: pointer;
      }
      .ncode {
        display: inline-block;
        width: 21px;
        height: 21px;
        margin-right: 3px;
        vertical-align: -4px;
        background-size: contain;
      }
    }
  }
`
