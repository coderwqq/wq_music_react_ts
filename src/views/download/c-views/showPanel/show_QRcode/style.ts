import styled from 'styled-components'

export const QRcodeWrapper = styled.div`
  position: absolute;
  bottom: 90px;
  left: 50%;
  transform: translateX(-50%);

  .show {
    position: relative;

    .show-QRcode {
      box-sizing: border-box;
      width: 204px;
      height: 224px;
      padding: 20px 18px 9px;
      background-color: #fff;
      border-radius: 10px;
      box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.5);

      img {
        width: 164px;
        height: 164px;
      }
      .text {
        font-size: 14px;
        color: #333;
        margin-top: 14px;
        text-align: center;
      }
    }
    .triangle-down {
      position: absolute;
      left: 50%;
      transform: translateX(-50%);
      bottom: -10px;
      border-width: 10px 10px 0;
      border-style: solid;
      border-left-color: transparent;
      border-right-color: transparent;
    }
  }
`
