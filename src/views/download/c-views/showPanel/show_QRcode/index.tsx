import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { QRcodeWrapper } from './style'

interface IProps {
  children?: ReactNode
}

const ShowQRcode: FC<IProps> = () => {
  return (
    <QRcodeWrapper>
      <div className="show">
        <div className="show-QRcode">
          <img
            src="https://p1.music.126.net/wCRnpjXHpGRiq1c1uCkIFQ==/109951164246717547.png"
            alt=""
          />
          <p className="text">扫描二维码下载</p>
        </div>
        <div className="triangle-down"></div>
      </div>
    </QRcodeWrapper>
  )
}

export default memo(ShowQRcode)
