import styled from 'styled-components'

export const OtherClientWrapper = styled.div`
  position: absolute;
  right: 0;
  top: 65px;

  > .inner {
    position: relative;
    box-sizing: border-box;
    width: 276px;
    height: 295px;
    background-color: #fff;
    border-radius: 12px;
    box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.5);

    .triangle-up {
      position: absolute;
      top: -10px;
      right: 45px;
      border-width: 0 10px 10px;
      border-style: solid;
      border-left-color: transparent;
      border-right-color: transparent;
    }
    .content {
      display: flex;
      flex-wrap: wrap;
      padding: 20px 0 0 10px;
    }
  }
`
interface IIcon {
  itemIcon: string
}
export const ClientIconWrapper = styled.a<IIcon>`
  float: left;
  display: block;
  margin: 0 0 10px 10px;
  width: 113px;
  height: 45px;
  line-height: 45px;
  background: rgba(0, 0, 0, 0.08);

  > i {
    float: left;
    margin: 10px 9px 0 9px;
    width: 26px;
    height: 26px;
    background: url(${(props) => props.itemIcon}) no-repeat;
    background-size: contain;
  }
  > em {
    color: #333;
    font-size: 12px;
    font-style: normal;
  }
`
