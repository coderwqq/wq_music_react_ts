import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { OtherClientWrapper, ClientIconWrapper } from './style'
import { clientIcon } from '@/assets/data/download_data'

interface IProps {
  children?: ReactNode
}

const OtherClient: FC<IProps> = () => {
  return (
    <OtherClientWrapper>
      <div className="inner">
        <div className="triangle-up"></div>
        <div className="content">
          {clientIcon.map((item, index) => {
            return (
              <ClientIconWrapper key={index} itemIcon={item.icon}>
                <i></i>
                <em className="text">{item.text}</em>
              </ClientIconWrapper>
            )
          })}
        </div>
      </div>
    </OtherClientWrapper>
  )
}

export default memo(OtherClient)
