import styled from 'styled-components'

interface IIntroduceV2 {
  itemData: any
}
export const IntroduceV2Wrapper = styled.div<IIntroduceV2>`
  height: 437px;
  border-top: 1px solid #e9e9e9;

  > .inner {
    display: flex;
    justify-content: space-between;
    margin: 0 auto;
    width: 982px;
    padding-top: ${(props) => props.itemData.padding_top};

    .image {
      margin-left: ${(props) => props.itemData.margin_left};
    }
    .info {
      .title {
        font-size: 40px;
        font-weight: 400;
        color: #333;
        padding: 45px 0 5px 0;
      }
      .text {
        font-size: 16px;
        color: #666;
        line-height: 24px;

        .text-red {
          color: #c00;
        }
      }
    }
    .sound {
      margin-top: 96px;
    }
  }
`
