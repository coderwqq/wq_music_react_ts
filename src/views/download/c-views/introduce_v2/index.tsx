import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { IntroduceV2Wrapper } from './style'

interface IProps {
  children?: ReactNode
  itemData: any
}

const IntroduceV2: FC<IProps> = (props) => {
  const { itemData } = props

  return (
    <IntroduceV2Wrapper itemData={itemData}>
      <div className="inner">
        <div className="image">
          <img src={itemData.img} alt="" />
        </div>
        {itemData.id == 222 && (
          <div className="info">
            <h3 className="title">{itemData.title}</h3>
            <p className="text">
              <span>{itemData.text1}</span>
              <span className="text-red">{itemData.text_red}</span>
              <span>{itemData.text2}</span>
              <br />
              <span>{itemData.text3}</span>
            </p>
          </div>
        )}
        {itemData.id == 444 && (
          <div className="info">
            <h3 className="title">{itemData.title}</h3>
            <p className="text">
              <span>{itemData.text1}</span>
              <br />
              <span>{itemData.text2}</span>
              <span className="text-red">{itemData.text_red}</span>
            </p>
          </div>
        )}
      </div>
    </IntroduceV2Wrapper>
  )
}

export default memo(IntroduceV2)
