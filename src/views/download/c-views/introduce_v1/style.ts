import styled from 'styled-components'

interface IIntroduceV1 {
  itemData: any
}
export const IntroduceV1Wrapper = styled.div<IIntroduceV1>`
  height: 437px;
  background-color: #fff;
  border-top: 1px solid #e9e9e9;

  > .inner {
    display: flex;
    justify-content: space-between;
    margin: 0 auto;
    padding-top: ${(props) => props.itemData.padding_top};
    width: 982px;

    .info {
      margin-left: 30px;

      .title {
        font-size: 40px;
        font-weight: 400;
        color: #333;
        padding: 45px 0 5px 0;
      }
      .text {
        font-size: 16px;
        color: #666;
        line-height: 24px;

        .text-red {
          color: #c00;
        }
      }
    }
    .sound {
      margin-top: 96px;
    }
    .image {
      margin-right: ${(props) => props.itemData.margin_right};
    }
  }
`
