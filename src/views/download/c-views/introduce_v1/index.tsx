import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { IntroduceV1Wrapper } from './style'

interface IProps {
  children?: ReactNode
  itemData: any
}

const IntroduceV1: FC<IProps> = (props) => {
  const { itemData } = props

  return (
    <IntroduceV1Wrapper itemData={itemData}>
      <div className="inner">
        {itemData.id == 111 && (
          <div className="info">
            <h3 className="title">{itemData.title}</h3>
            <p className="text">
              <span>{itemData.text1}</span>
              <br />
              <span>{itemData.text2}</span>
            </p>
          </div>
        )}
        {itemData.id == 333 && (
          <div className="info">
            <h3 className="title">{itemData.title}</h3>
            <p className="text">
              <span>{itemData.text1}</span>
              <span className="text-red">{itemData.text_red}</span>
              <span>{itemData.text2}</span>
              <br />
              <span>{itemData.text3}</span>
            </p>
          </div>
        )}
        {itemData.id == 555 && (
          <div className="info sound">
            <h3 className="title">{itemData.title}</h3>
            <p className="text">
              <span>{itemData.text1}</span>
              <br />
              <span>{itemData.text2}</span>
              <span className="text-red">{itemData.text_red}</span>
              <span>{itemData.text3}</span>
            </p>
          </div>
        )}
        <div className="image">
          <img src={itemData.img} alt="" />
        </div>
      </div>
    </IntroduceV1Wrapper>
  )
}

export default memo(IntroduceV1)
