import styled from 'styled-components'

export const DownloadWrapper = styled.div`
  .QRcode {
    position: fixed;
    top: 400px;
    right: 25px;
    padding: 16px 18px 9px;
    background-color: #fff;
    border-radius: 10px;
    box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.5);

    img {
      width: 100px;
      height: 100px;
    }
    .text {
      font-size: 12px;
      color: #333;
      margin-top: 10px;
      text-align: center;
    }
  }
`
