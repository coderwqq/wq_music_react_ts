import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { FocusWrapper } from './style'

interface IProps {
  children?: ReactNode
}

const Focus: FC<IProps> = () => {
  return (
    <FocusWrapper>
      <div className="friend_sprite focus">
        <a className="friend_sprite login">立即登录</a>
      </div>
      <p className="text">
        <span className="span">你可以关注明星和好友品味他们的私房歌单</span>
        <br />
        <span className="span">通过他们的动态发现更多精彩音乐</span>
      </p>
    </FocusWrapper>
  )
}

export default memo(Focus)
