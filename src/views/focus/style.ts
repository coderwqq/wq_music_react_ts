import styled from 'styled-components'

export const FocusWrapper = styled.div`
  margin: 0 auto;
  width: 980px;
  min-height: 700px;
  background-color: #fff;
  border: 1px solid #d3d3d3;

  .focus {
    margin: 0 auto;
    width: 902px;
    height: 414px;
    padding-top: 70px;
    background-position: 0 70px;
    text-indent: -9999px;

    .login {
      display: block;
      width: 157px;
      height: 48px;
      margin: 260px 0 0 535px;
      background-position: 0 -430px;
    }
  }
  .text {
    position: relative;
    top: -248px;
    left: 572px;
    width: 367px;
    color: #666;

    .span {
      line-height: 22px;
    }
  }
`
