import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'
import { NavLink } from 'react-router-dom'

import { NavWrapper } from './style'
import { discoverNav } from '@/assets/data/local_data'

interface IProps {
  children?: ReactNode
}

const NavBar: FC<IProps> = () => {
  return (
    <NavWrapper>
      <div className="nav wrapV1">
        {discoverNav.map((item) => {
          return (
            <div className="item" key={item.title}>
              <NavLink to={item.link}>{item.title}</NavLink>
            </div>
          )
        })}
      </div>
    </NavWrapper>
  )
}

export default memo(NavBar)
