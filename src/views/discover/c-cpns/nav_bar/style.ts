import styled from 'styled-components'

export const NavWrapper = styled.div`
  height: 30px;
  background-color: ${(props) => props.theme.color.primary};

  .nav {
    box-sizing: border-box;
    display: flex;
    padding-left: 183px;

    .item {
      margin: 3px 17px 0;

      a {
        display: inline-block;
        font-size: 12px;
        padding: 0 13px;
        color: #fff;
        height: 20px;
        line-height: 19px;

        &.active,
        &:hover {
          background-color: #9b0909;
          border-radius: 10px;
        }
      }
    }
  }
`
