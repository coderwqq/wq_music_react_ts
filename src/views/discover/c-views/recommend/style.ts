import styled from 'styled-components'

export const RecommentWrapper = styled.div`
  > .content {
    position: relative;
    display: flex;
    width: 982px;
    left: 2px;
    box-sizing: border-box;
    border: 1px solid #d3d3d3;
    background: url(${require('@/assets/img/wrap-bg.png')});

    > .left {
      box-sizing: border-box;
      width: 730px;
      padding: 20px 20px 40px;
    }
    > .right {
      width: 252px;
    }
  }
`
