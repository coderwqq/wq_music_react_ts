import styled from 'styled-components'

export const RankingWrapper = styled.div`
  .background {
    margin-top: 20px;
    height: 472px;
    background-position: 0 0;

    .ranking-inner {
      display: flex;
    }
  }
`
