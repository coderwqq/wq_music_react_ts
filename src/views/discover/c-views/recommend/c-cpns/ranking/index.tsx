import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'
import { shallowEqual } from 'react-redux'

import { useAppSelector } from '@/store'
import { RankingWrapper } from './style'
import SectionHeader from '@/components/section_header'
import RankingItem from '../ranking_item'

interface IProps {
  children?: ReactNode
}

const Ranking: FC<IProps> = () => {
  const { ranking } = useAppSelector(
    (state) => ({
      ranking: state.recommendReducer.ranking
    }),
    shallowEqual
  )

  return (
    <RankingWrapper>
      <SectionHeader
        title="榜单"
        moreText="更多"
        moreLink="/discover/ranking"
      />
      <div className="background top-ranking-bg">
        <div className="ranking-inner">
          {ranking.map((item) => {
            return <RankingItem key={item.id} itemData={item} />
          })}
        </div>
      </div>
    </RankingWrapper>
  )
}

export default memo(Ranking)
