import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'
import { Link } from 'react-router-dom'

import { RankingItemWrapper, TopWrapper, ListWrapper } from './style'
import { getImageSize } from '@/utils/format'
import { useAppDispatch } from '@/store'
import { fetchCurrentSongAction } from '@/store/modules/player'

interface IProps {
  children?: ReactNode
  itemData: any
}

const RankingItem: FC<IProps> = (props) => {
  const { itemData } = props
  const { tracks = [] } = itemData

  const dispatch = useAppDispatch()
  function playBtnClick(id: number) {
    console.log('id', id)
    dispatch(fetchCurrentSongAction(id))
  }

  return (
    <RankingItemWrapper>
      <TopWrapper>
        <div className="r-left ">
          <img
            src={getImageSize(itemData.coverImgUrl, 80)}
            alt={itemData.name}
          />
          <Link
            to={'/discover/ranking'}
            className="r-cover sprite_cover"
          ></Link>
        </div>
        <div className="r-right">
          <h4 className="r-name">{itemData.name}</h4>
          <div className="r-icon">
            <i className="sprite_02 icon play"></i>
            <i className="sprite_02 icon collect"></i>
          </div>
        </div>
      </TopWrapper>
      <ListWrapper>
        {tracks.slice(0, 10).map((item: any, index: number) => {
          return (
            <li key={item.id} className="r-list">
              <span className="r-index">{index + 1}</span>
              <Link className="r-song" to={''}>
                {item.name}
              </Link>
              <div className="oper">
                <a
                  className="o-icon sprite_02 o-play"
                  onClick={() => playBtnClick(item.id)}
                ></a>
                <a className="o-icon sprite_icon2 o-add"></a>
                <a className="o-icon sprite_02 o-favor"></a>
              </div>
            </li>
          )
        })}
      </ListWrapper>
      <div className="more">
        <Link to={'/discover/ranking'} className="more-text">
          查看全部&gt;
        </Link>
      </div>
    </RankingItemWrapper>
  )
}

export default memo(RankingItem)
