import styled from 'styled-components'

export const RankingItemWrapper = styled.div`
  width: 230px;

  :last-child {
    width: 228px;
  }
  .more {
    margin-right: 32px;
    line-height: 32px;
    text-align: right;

    .more-text {
      font-size: 12px;
      color: #000;

      &:hover {
        text-decoration: underline;
      }
    }
  }
`
export const TopWrapper = styled.div`
  display: flex;
  height: 100px;
  padding: 20px 0 0 20px;

  .r-left {
    position: relative;

    .r-cover {
      position: absolute;
      left: 0;
      top: 0;
      width: 80px;
      height: 80px;
      background-position: -145px -57px;
      cursor: pointer;
    }
  }
  .r-right {
    margin: 6px 0 0 10px;
    color: #333;
    cursor: pointer;

    .r-name {
      &:hover {
        text-decoration: underline;
      }
    }
    .r-icon {
      margin-top: 10px;

      .icon {
        display: inline-block;
        width: 22px;
        height: 22px;
        cursor: pointer;
      }
      .play {
        margin-right: 10px;
        background-position: -267px -205px;

        &:hover {
          background-position: -267px -235px;
        }
      }
      .collect {
        background-position: -300px -205px;

        &:hover {
          background-position: -300px -235px;
        }
      }
    }
  }
`
export const ListWrapper = styled.div`
  .r-list {
    height: 32px;
    line-height: 32px;
    margin-left: 17px;

    .r-index {
      float: left;
      width: 35px;
      text-align: center;
      font-size: 16px;
      color: #666;
    }
    :nth-child(-n + 3) .r-index {
      color: #c10d0c;
    }
    .r-song {
      float: left;
      width: 170px;
      font-size: 12px;
      color: #000;
      ${(props) => props.theme.mixin.writeNowrap}

      &:hover {
        text-decoration: underline;
      }
    }

    &:hover {
      .r-song {
        width: 94px;
      }
      .oper {
        display: flex;
      }
    }
    .oper {
      display: none;
      float: right;
      align-items: center;
      width: 82px;
      height: 32px;

      .o-icon {
        display: inline-block;
        width: 17px;
        height: 17px;
        margin-right: 10px;
      }
      .o-play {
        background-position: -267px -268px;

        &:hover {
          background-position: -267px -288px;
        }
      }
      .o-add {
        position: relative;
        top: 2px;
        background-position: 0 -700px;

        &:hover {
          background-position: -22px -700px;
        }
      }
      .o-favor {
        background-position: -297px -268px;

        &:hover {
          background-position: -297px -288px;
        }
      }
    }
  }
`
