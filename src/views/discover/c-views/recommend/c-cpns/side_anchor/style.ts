import styled from 'styled-components'

export const AnchorWrapper = styled.div`
  margin-top: 30px;

  .s-content {
    margin: 20px 0 0 20px;

    > .li {
      display: flex;

      font-size: 12px;
      margin-bottom: 8px;

      .left {
        margin-right: 10px;
      }
      .right {
        width: 160px;
        line-height: 21px;

        .name {
          color: #000;

          &:hover {
            text-decoration: underline;
          }
        }
        .intro {
          color: #666;
          ${(props) => props.theme.mixin.writeNowrap}
        }
      }
    }
  }
`
