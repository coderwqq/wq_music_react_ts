import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'
import { Link } from 'react-router-dom'

import { AnchorWrapper } from './style'
import SideHeader from '../side_header'
import { hotRadios } from '@/assets/data/local_data'
import { getImageSize } from '@/utils/format'

interface IProps {
  children?: ReactNode
}

const SideAnchor: FC<IProps> = () => {
  return (
    <AnchorWrapper>
      <SideHeader title="热门主播" />
      <ul className="s-content">
        {hotRadios.map((item) => {
          return (
            <li key={item.picUrl} className="li">
              <div className="left">
                <img src={getImageSize(item.picUrl, 40)} alt="" />
              </div>
              <div className="right">
                <Link to={'/'} className="name">
                  {item.name}
                </Link>
                <div className="intro">{item.position}</div>
              </div>
            </li>
          )
        })}
      </ul>
    </AnchorWrapper>
  )
}

export default memo(SideAnchor)
