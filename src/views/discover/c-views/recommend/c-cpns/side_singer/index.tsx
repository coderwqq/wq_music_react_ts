import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'
import { shallowEqual } from 'react-redux'

import { useAppSelector } from '@/store'
import { SingerWrapper } from './style'
import SideHeader from '../side_header'
import SideSingerItem from '../side_singer_item'

interface IProps {
  children?: ReactNode
}

const SideSinger: FC<IProps> = () => {
  const { settledSinger } = useAppSelector(
    (state) => ({
      settledSinger: state.recommendReducer.settledSinger
    }),
    shallowEqual
  )

  return (
    <SingerWrapper>
      <SideHeader title="入驻歌手" more="查看全部&gt;" />
      <div className="singer-item">
        {settledSinger.map((item) => {
          return <SideSingerItem key={item.id} itemData={item} />
        })}
      </div>
      <a className="musician">申请成为网易音乐人</a>
    </SingerWrapper>
  )
}

export default memo(SideSinger)
