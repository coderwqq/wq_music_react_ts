import styled from 'styled-components'

export const SingerWrapper = styled.div`
  .singer-item {
    margin: 6px 0 14px 20px;
  }
  .musician {
    display: block;
    margin: 0 auto;
    width: 210px;
    height: 31px;
    line-height: 31px;
    font-weight: 700;
    text-align: center;
    color: #333;
    background: #fafafa;
    border: 1px solid #c3c3c3;
    border-radius: 4px;
  }
`
