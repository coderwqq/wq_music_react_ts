import React, { memo, FC, useRef, ElementRef } from 'react'
import type { ReactNode } from 'react'
import { shallowEqual } from 'react-redux'
import { Carousel } from 'antd'

import { AlbumWrapper } from './style'
import { useAppSelector } from '@/store'
import SectionHeader from '@/components/section_header'
import NewAlbumItem from '@/components/newAlbum_item'

interface IProps {
  children?: ReactNode
}

const NewAlbum: FC<IProps> = () => {
  const carouselRef = useRef<ElementRef<typeof Carousel>>(null)
  const { newAlbums } = useAppSelector(
    (state) => ({
      newAlbums: state.recommendReducer.newAlbums
    }),
    shallowEqual
  )

  function handleBtnClick(isNext: boolean) {
    if (isNext) {
      carouselRef.current?.next()
    } else {
      carouselRef.current?.prev()
    }
  }

  return (
    <AlbumWrapper>
      <SectionHeader
        title="新碟上架"
        moreText="更多"
        moreLink="/discover/album"
      />
      <div className="album-content">
        <div className="inner">
          <button
            className="sprite_02 arrow arrow-left"
            onClick={() => handleBtnClick(false)}
          ></button>
          <div className="album-banner">
            <Carousel
              dots={false}
              speed={1500}
              ref={carouselRef}
              className="carousel"
            >
              {[0, 1].map((item) => {
                return (
                  <div key={item} className="item">
                    <div className="album-item">
                      {newAlbums
                        ?.slice(item * 5, (item + 1) * 5)
                        .map((album) => {
                          return (
                            <NewAlbumItem key={album.id} itemData={album} />
                          )
                        })}
                    </div>
                  </div>
                )
              })}
            </Carousel>
          </div>
          <button
            className="sprite_02 arrow arrow-right"
            onClick={() => handleBtnClick(true)}
          ></button>
        </div>
      </div>
    </AlbumWrapper>
  )
}

export default memo(NewAlbum)
