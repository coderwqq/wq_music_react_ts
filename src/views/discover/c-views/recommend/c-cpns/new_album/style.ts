import styled from 'styled-components'

export const AlbumWrapper = styled.div`
  margin-top: 35px;

  .album-content {
    margin: 20px 0 37px;
    border: 1px solid #d3d3d3;

    .inner {
      display: flex;
      height: 184px;
      background-color: #f5f5f5;
      border: 1px solid #fff;

      .arrow {
        position: relative;
        top: 70px;
        width: 17px;
        height: 17px;
      }
      .arrow-left {
        left: 4px;
        background-position: -260px -75px;

        &:hover {
          background-position: -280px -75px;
        }
      }
      .arrow-right {
        right: 4px;
        background-position: -300px -75px;

        &:hover {
          background-position: -320px -75px;
        }
      }
      .album-banner {
        flex: 1;
        overflow: hidden;

        .album-item {
          display: flex;
          margin-top: 28px;
        }
      }
    }
  }
`
