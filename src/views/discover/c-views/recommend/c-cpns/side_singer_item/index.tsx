import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { SingerItemWrapper } from './style'
import { getImageSize } from '@/utils/format'

interface IProps {
  children?: ReactNode
  itemData: any
}

const SideSingerItem: FC<IProps> = (props) => {
  const { itemData } = props

  return (
    <SingerItemWrapper>
      <div className="singer-left">
        <img src={getImageSize(itemData.img1v1Url, 62)} alt="" />
      </div>
      <div className="singer-right">
        <div className="s-name">{itemData.name}</div>
        <div className="s-alias">{itemData.alias.join(' ')}</div>
      </div>
    </SingerItemWrapper>
  )
}

export default memo(SideSingerItem)
