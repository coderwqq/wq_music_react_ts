import styled from 'styled-components'

export const SingerItemWrapper = styled.div`
  display: flex;
  margin-top: 14px;
  width: 210px;
  height: 62px;
  background: #fafafa;
  cursor: pointer;

  .singer-right {
    box-sizing: border-box;
    width: 148px;
    height: 62px;
    padding-left: 14px;
    border: 1px solid #e9e9e9;
    border-left: none;

    .s-name {
      margin-top: 12px;
      font-weight: 700;
      color: #333;
    }
    .s-alias {
      margin-top: 8px;
      width: 90%;
      font-size: 12px;
      color: #666;
      ${(props) => props.theme.mixin.writeNowrap}
    }
  }
`
