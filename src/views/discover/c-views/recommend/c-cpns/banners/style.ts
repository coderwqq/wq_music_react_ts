import styled from 'styled-components'

export const BannersWrapper = styled.div`
  .top {
    position: relative;
    display: flex;
  }
`
export const LeftWrapper = styled.div`
  position: relative;
  width: 730px;
  height: 285px;

  .banner-item {
    width: 100%;
    height: 285px;
    overflow: hidden;

    .image {
      width: 100%;
      height: 285px;
    }
  }
  .dots {
    position: absolute;
    display: flex;
    left: 50%;
    bottom: 10px;
    margin-left: -96px;

    li {
      user-select: none;

      .item {
        display: inline-block;
        margin: 0 9px;
        width: 6px;
        height: 6px;
        border-radius: 50%;
        background-color: #fff;
        cursor: pointer;
      }
      .active {
        background-color: #f00;
      }
    }
  }
`
export const RightWrapper = styled.div`
  height: 285px;

  a {
    display: inline-block;
    width: 254px;
    height: 285px;
    background-image: url(${require('@/assets/img/download.png')});
  }
  .client {
    position: relative;
    bottom: 45px;
    margin: 10px auto;
    font-size: 12px;
    color: #8d8d8d;
    text-align: center;
  }
`
export const ControlWrapper = styled.div`
  .btn {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    width: 37px;
    height: 63px;
    cursor: pointer;

    &:hover {
      background-color: rgba(0, 0, 0, 0.1);
    }
  }
  .left {
    left: -68px;
    background-image: url(${require('@/assets/img/banner-control-left.png')});
    background-color: transparent;
  }
  .right {
    right: -68px;
    background-image: url(${require('@/assets/img/banner-control-right.png')});
    background-color: transparent;
  }
`
