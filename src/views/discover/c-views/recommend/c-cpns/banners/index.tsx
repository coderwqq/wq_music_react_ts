import React, { memo, FC, useState, useRef } from 'react'
import type { ReactNode, ElementRef } from 'react'
import { shallowEqual } from 'react-redux'
import { Link } from 'react-router-dom'
import { Carousel } from 'antd'
import classNames from 'classnames'

import {
  BannersWrapper,
  ControlWrapper,
  LeftWrapper,
  RightWrapper
  // RightWrapper
} from './style'
import { useAppSelector } from '@/store'

interface IProps {
  children?: ReactNode
}

const Banners: FC<IProps> = () => {
  const [currentIndex, setCurrentIndex] = useState(0)
  const bannerRef = useRef<ElementRef<typeof Carousel>>(null)

  const { banners } = useAppSelector(
    (state) => ({
      banners: state.recommendReducer.banners
    }),
    shallowEqual
  )

  function handleAfterChange(current: number) {
    setCurrentIndex(current)
  }

  function isNextClick(isNext: boolean) {
    if (isNext) {
      bannerRef.current?.next()
    } else {
      bannerRef.current?.prev()
    }
  }

  // 获取背景图片
  let bgImageUrl
  if (currentIndex >= 0 && banners?.length > 0) {
    bgImageUrl = banners[currentIndex].imageUrl + '?imageView&blur=40x20'
  }

  return (
    <BannersWrapper
      style={{ background: `url('${bgImageUrl}') center center /6000px` }}
    >
      <div className="top wrapV2">
        <LeftWrapper>
          <Carousel
            autoplay
            afterChange={handleAfterChange}
            effect="fade"
            dots={false}
            // dots={{ className: 'dot' }}
            ref={bannerRef}
          >
            {banners.map((item) => {
              return (
                <div className="banner-item" key={item.targetId}>
                  <img className="image" src={item.imageUrl} alt="" />
                </div>
              )
            })}
          </Carousel>
          <ul className="dots">
            {banners.map((item, index) => {
              return (
                <li key={item.imageUrl}>
                  <span
                    className={classNames('item', {
                      active: index === currentIndex
                    })}
                  ></span>
                </li>
              )
            })}
          </ul>
        </LeftWrapper>
        <RightWrapper>
          <Link to="/download"></Link>
          <p className="client">PC 安卓 iPhone WP iPad Mac 六大客户端</p>
        </RightWrapper>
        <ControlWrapper>
          <button
            className="btn left"
            onClick={() => isNextClick(false)}
          ></button>
          <button
            className="btn right"
            onClick={() => isNextClick(true)}
          ></button>
        </ControlWrapper>
      </div>
    </BannersWrapper>
  )
}

export default memo(Banners)
