import styled from 'styled-components'

export const HeaderWrapper = styled.div`
  display: flex;
  margin: 15px auto 0;
  width: 210px;
  height: 23px;
  font-size: 12px;
  border-bottom: 1px solid #ccc;

  .settled {
    flex: 1;
    color: #333;
  }
  .all {
    color: #666;

    &:hover {
      text-decoration: underline;
    }
  }
`
