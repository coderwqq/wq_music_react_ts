import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { HeaderWrapper } from './style'

interface IProps {
  children?: ReactNode
  title: string
  more?: string
}

const SideHeader: FC<IProps> = (props) => {
  const { title, more } = props

  return (
    <HeaderWrapper>
      <h4 className="settled">{title}</h4>
      {more && <a className="all">{more}</a>}
    </HeaderWrapper>
  )
}

export default memo(SideHeader)
