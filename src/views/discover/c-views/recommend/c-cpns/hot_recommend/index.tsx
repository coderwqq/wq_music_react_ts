import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { useAppSelector } from '@/store'
import { HotWrapper } from './style'
import SectionHeader from '@/components/section_header'
import SongsItem from '@/components/songs_item'
import { shallowEqual } from 'react-redux'

interface IProps {
  children?: ReactNode
}

const HotRecommend: FC<IProps> = () => {
  const { hotRecommend } = useAppSelector(
    (state) => ({
      hotRecommend: state.recommendReducer.hotRecommend
    }),
    shallowEqual
  )

  return (
    <HotWrapper>
      <SectionHeader
        title="热门推荐"
        keyWords={['华语', '流行', '摇滚', '民谣', '电子']}
        moreText="更多"
        moreLink="/discover/playlist"
      />
      <div className="songs-list">
        {hotRecommend?.map((item) => {
          return <SongsItem key={item.id} itemData={item} />
        })}
      </div>
    </HotWrapper>
  )
}

export default memo(HotRecommend)
