import styled from 'styled-components'

export const HotWrapper = styled.div`
  .songs-list {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    margin-top: 20px;
  }
`
