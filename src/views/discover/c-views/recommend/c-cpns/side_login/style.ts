import styled from 'styled-components'

export const LoginWrapper = styled.div`
  height: 126px;
  background-position: 0 0;

  .desc {
    padding: 16px 0;
    margin: 0 auto;
    width: 205px;
    font-size: 12px;
    line-height: 22px;
    color: #666;
  }
  .login {
    display: block;
    margin: 0 auto;
    width: 100px;
    height: 31px;
    line-height: 31px;
    color: #fff;
    text-align: center;
    background-position: 0 -195px;
  }
`
