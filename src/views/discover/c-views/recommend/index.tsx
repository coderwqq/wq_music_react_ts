import React, { memo, FC, useEffect } from 'react'
import type { ReactNode } from 'react'

import { useAppDispatch } from '@/store'
import {
  fetchRankingDataAction,
  fetchRecommentDataAction
} from '@/store/modules/discover/recommend'
import { RecommentWrapper } from './style'
import Banners from './c-cpns/banners'
import HotRecommend from './c-cpns/hot_recommend'
import NewAlbum from './c-cpns/new_album'
import Ranking from './c-cpns/ranking'
import SideLogin from './c-cpns/side_login'
import SideSinger from './c-cpns/side_singer'
import SideAnchor from './c-cpns/side_anchor'

interface IProps {
  children?: ReactNode
}

const Recommend: FC<IProps> = () => {
  const dispatch = useAppDispatch()
  useEffect(() => {
    dispatch(fetchRecommentDataAction())
    dispatch(fetchRankingDataAction())
  }, [])

  return (
    <RecommentWrapper>
      <Banners />
      <div className="content wrapV2">
        <div className="left">
          <HotRecommend />
          <NewAlbum />
          <Ranking />
        </div>
        <div className="right">
          <SideLogin />
          <SideSinger />
          <SideAnchor />
        </div>
      </div>
    </RecommentWrapper>
  )
}

export default memo(Recommend)
