import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { PagingWrapper } from './style'
import Paging from '@/components/pagination'

interface IProps {
  children?: ReactNode
}

const Pagination: FC<IProps> = () => {
  return (
    <PagingWrapper>
      <Paging />
    </PagingWrapper>
  )
}

export default memo(Pagination)
