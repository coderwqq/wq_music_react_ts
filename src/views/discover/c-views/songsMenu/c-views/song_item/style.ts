import styled from 'styled-components'

export const SongItemWrapper = styled.li`
  box-sizing: border-box;
  margin: 0 0 30px 50px;
  width: 140px;

  > .frame {
    position: relative;

    img {
      display: block;
      height: 140px;
    }
    .crown {
      position: absolute;
      top: 0;
      left: 0;
      width: 40px;
      height: 40px;
      background-position: -135px -220px;
    }
    .cover {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-position: 0 0;
    }
    .bottom {
      position: absolute;
      left: 0;
      bottom: 0;
      width: 100%;
      height: 27px;
      display: flex;
      justify-content: space-between;
      align-items: center;
      background-position: 0 -537px;

      .left {
        i {
          display: inline-block;
          width: 14px;
          height: 11px;
          margin: 4px 5px 0 10px;
          background-position: 0 -24px;
        }
        .playCount {
          color: #ccc;
          font-size: 12px;
        }
      }
      .right {
        i {
          display: inline-block;
          width: 16px;
          height: 17px;
          margin: 4px 10px 0 0;
          background-position: 0 0;
          cursor: pointer;
        }
      }
    }
  }
  > .message {
    .intro {
      padding: 10px 0 7px;
      color: #000;
      ${(props) => props.theme.mixin.writeNowrap}

      &:hover {
        text-decoration: underline;
        cursor: pointer;
      }
    }
    .name {
      font-size: 12px;
      color: #999;

      .nickname {
        margin-left: 3px;
        color: #666;
        ${(props) => props.theme.mixin.writeNowrap}
      }
    }
  }
`
