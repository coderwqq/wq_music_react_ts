import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { SongItemWrapper } from './style'
import { getImageSize } from '@/utils/format'
import { formatCount } from '@/utils/format'

interface IProps {
  children?: ReactNode
  itemData: any
}

const SongItem: FC<IProps> = (props) => {
  const { itemData } = props

  return (
    <SongItemWrapper>
      <div className="frame">
        <img src={getImageSize(itemData?.coverImgUrl, 140)} alt="" />
        <div className="sprite_icon3 crown"></div>
        <div className="sprite_cover cover"></div>
        <div className="sprite_cover bottom">
          <div className="left">
            <i className="sprite_icon"></i>
            <span className="playCount">
              {formatCount(itemData?.playCount)}
            </span>
          </div>
          <div className="right">
            <i className="sprite_icon"></i>
          </div>
        </div>
      </div>
      <div className="message">
        <p className="intro">{itemData?.name}</p>
        <div className="name">
          <span>by</span>
          <span className="nickname">{itemData?.creator?.nickname}</span>
        </div>
      </div>
    </SongItemWrapper>
  )
}

export default memo(SongItem)
