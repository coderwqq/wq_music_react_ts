import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { HeaderWrapper } from './style'

interface IProps {
  children?: ReactNode
}

const Header: FC<IProps> = () => {
  return (
    <HeaderWrapper>
      <div className="left">
        <h3 className="title">全部</h3>
        <a className="sprite_button">
          <i className="sprite_button">
            选择分类
            <em className="sprite_icon2"></em>
          </i>
        </a>
      </div>
      <div className="right sprite_button2">热门</div>
    </HeaderWrapper>
  )
}

export default memo(Header)
