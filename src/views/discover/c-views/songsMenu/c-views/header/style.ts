import styled from 'styled-components'

export const HeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  height: 40px;
  border-bottom: 2px solid #c20c0c;

  > .left {
    display: flex;

    .title {
      margin-top: 3px;
      font-size: 24px;
      font-weight: 400;
    }
    a {
      padding-right: 5px;
      margin: 2px 0 0 12px;
      background-position: right -100px;

      i {
        display: inline-block;
        padding: 0 10px 0 15px;
        height: 31px;
        line-height: 31px;
        font-style: normal;
        font-size: 12px;
        color: #0c73c2;
        background-position: 0 -59px;

        em {
          display: inline-block;
          margin-left: 5px;
          width: 8px;
          height: 5px;
          vertical-align: middle;
          background-position: -70px -543px;
        }
      }
    }
  }
  > .right {
    color: #fff;
    width: 46px;
    height: 29px;
    line-height: 29px;
    text-align: center;
    background-position: 0 0;
    border-radius: 3px;
    cursor: pointer;
  }
`
