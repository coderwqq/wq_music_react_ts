import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { SongsWrapper } from './style'
import { useAppSelector } from '@/store'
import SongItem from '../song_item'
import { shallowEqual } from 'react-redux'

interface IProps {
  children?: ReactNode
}

const Songs: FC<IProps> = () => {
  const { categorySongs } = useAppSelector(
    (state) => ({
      categorySongs: state.songsMenuReducer.categorySongs
    }),
    shallowEqual
  )

  return (
    <SongsWrapper>
      {categorySongs?.playlists?.map((item: any) => {
        return <SongItem key={item.id} itemData={item} />
      })}
    </SongsWrapper>
  )
}

export default memo(Songs)
