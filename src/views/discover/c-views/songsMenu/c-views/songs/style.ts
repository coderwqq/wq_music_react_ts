import styled from 'styled-components'

export const SongsWrapper = styled.ul`
  display: flex;
  flex-wrap: wrap;
  margin: 30px 0 0 -50px;
`
