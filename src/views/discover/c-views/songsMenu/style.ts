import styled from 'styled-components'

export const SongsMenuWrapper = styled.div`
  position: relative;

  > .inner {
    box-sizing: border-box;
    padding: 39px;
    background-color: #fff;
    border: 1px solid #d3d3d3;
    border-width: 0 1px;
  }
  > .cover {
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    background-color: rgba(0, 0, 0, 0.5);
  }
`
