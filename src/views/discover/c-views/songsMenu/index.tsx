import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { useAppDispatch } from '@/store'
import { fetchSongsMenuDataAction } from '@/store/modules/discover/songsMenu'
import { SongsMenuWrapper } from './style'
import Header from './c-views/header'
import Songs from './c-views/songs'
import Paging from './c-views/pagination'

interface IProps {
  children?: ReactNode
}

const SongsMenu: FC<IProps> = () => {
  const dispatch = useAppDispatch()
  dispatch(fetchSongsMenuDataAction(1))

  return (
    <SongsMenuWrapper>
      <div className="inner wrapV2">
        <Header />
        <Songs />
        <Paging />
      </div>
    </SongsMenuWrapper>
  )
}

export default memo(SongsMenu)
