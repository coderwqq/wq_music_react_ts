import styled from 'styled-components'

export const ItemWrapper = styled.div`
  display: flex;
  padding: 10px 0 10px 20px;
  cursor: pointer;

  .item-left {
    height: 42px;
    margin-right: 10px;
  }
  .item-right {
    font-size: 12px;

    .name {
      margin: 2px 0 10px;
      a {
        color: #000;
        &:hover {
          text-decoration: underline;
        }
      }
    }
    .text {
      color: #999;
      vertical-align: bottom;
    }
  }
`
