import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'
import { Link } from 'react-router-dom'

import { ItemWrapper } from './style'
import { getImageSize } from '@/utils/format'

interface IProps {
  children?: ReactNode
  itemData: any
}

const SideItem: FC<IProps> = (props) => {
  const { itemData } = props

  return (
    <ItemWrapper>
      <div className="item-left">
        <Link to={`/discover/toplist?id=${itemData.id}`}>
          <img src={getImageSize(itemData?.coverImgUrl, 40)} alt="" />
        </Link>
      </div>
      <div className="item-right">
        <p className="name">
          <Link to={`/discover/toplist?id=${itemData.id}`}>
            {itemData.name}
          </Link>
        </p>
        <p className="text">{itemData.updateFrequency}</p>
      </div>
    </ItemWrapper>
  )
}

export default memo(SideItem)
