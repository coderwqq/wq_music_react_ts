import styled from 'styled-components'

export const HeaderBarWrapper = styled.div`
  display: flex;

  .btn {
    display: inline-block;
  }
  .blue {
    width: 66px;
    height: 31px;
    background-position: right -428px;

    i {
      display: flex;
      align-items: center;
      padding: 0 7px 0 8px;
      width: 61px;
      height: 31px;
      color: #fff;
      font-style: normal;
      background-position: 0 -387px;

      .play {
        margin-right: 2px;
        width: 20px;
        height: 18px;
        background-position: 0 -1622px;
      }
    }
  }
  .add {
    margin-right: 6px;
    width: 31px;
    height: 31px;
    background-position: 0 -1588px;
  }
  .white {
    margin-right: 6px;
    padding-right: 5px;
    background-position: right -1020px;

    i {
      display: flex;
      align-items: center;
      padding: 1px 2px 0 28px;
      min-width: 23px;
      height: 31px;
      font-style: normal;
      font-size: 12px;
    }
    .favor {
      background-position: 0 -977px;
    }
    .share {
      background-position: 0 -1225px;
    }
    .download {
      background-position: 0 -2761px;
    }
    .comment {
      background-position: 0 -1465px;
    }
  }
`
