import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { HeaderBarWrapper } from './style'

interface IProps {
  children?: ReactNode
  itemData: any
}

const HeaderBar: FC<IProps> = (props) => {
  const { itemData } = props

  return (
    <HeaderBarWrapper>
      <a className="sprite_button btn blue">
        <i className="sprite_button btn">
          <em className="sprite_button btn play"></em>
          播放
        </i>
      </a>
      <a className="sprite_button btn add"></a>
      <a className="sprite_button btn white">
        <i className="sprite_button btn favor">({itemData?.subscribedCount})</i>
      </a>
      <a className="sprite_button btn white">
        <i className="sprite_button btn share">({itemData?.shareCount})</i>
      </a>
      <a className="sprite_button btn white">
        <i className="sprite_button btn download">下载</i>
      </a>
      <a className="sprite_button btn white">
        <i className="sprite_button btn comment">({itemData?.commentCount})</i>
      </a>
    </HeaderBarWrapper>
  )
}

export default memo(HeaderBar)
