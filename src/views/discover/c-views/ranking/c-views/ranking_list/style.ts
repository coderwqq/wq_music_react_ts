import styled from 'styled-components'

export const ListWrapper = styled.div`
  padding: 0 30px 40px 40px;

  > .header {
    display: flex;
    justify-content: space-between;
    height: 35px;
    line-height: 35px;
    border-bottom: 2px solid #c20c0c;

    .left {
      .title {
        font-size: 20px;
      }
      .trackCount {
        font-size: 12px;
        color: #666;
        padding: 9px 0 0 20px;
      }
    }
    .right {
      color: #666;
      font-size: 12px;

      .playCount {
        color: #c20c0c;
      }
    }
  }
`
