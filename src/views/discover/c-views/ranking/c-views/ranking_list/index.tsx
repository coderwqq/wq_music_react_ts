import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { ListWrapper } from './style'
import { useAppSelector } from '@/store'
import RankingListItem from '../ranking_list_item'
import { shallowEqual } from 'react-redux'

interface IProps {
  children?: ReactNode
}

const RankingList: FC<IProps> = () => {
  const { playlist } = useAppSelector(
    (state) => ({
      playlist: state.rankingReducer.playlist
    }),
    shallowEqual
  )

  return (
    <ListWrapper>
      <div className="header">
        <div className="left">
          <span className="title">歌曲列表</span>
          <span className="trackCount">{playlist.trackCount}首歌</span>
        </div>
        <div className="right">
          <span>播放: </span>
          <strong className="playCount">{playlist.playCount}</strong>
          <span>次</span>
        </div>
      </div>
      <RankingListItem itemData={playlist?.tracks} />
    </ListWrapper>
  )
}

export default memo(RankingList)
