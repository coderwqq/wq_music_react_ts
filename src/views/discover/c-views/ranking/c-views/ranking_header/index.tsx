import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { HeaderWrapper, LeftWrapper, RightWrapper } from './style'
import { useAppSelector } from '@/store'
import { getImageSize } from '@/utils/format'
import HeaderBar from '../header_bar'
import { formatMonthDay } from '@/utils/format'
import { shallowEqual } from 'react-redux'

interface IProps {
  children?: ReactNode
}

const RankingHeader: FC<IProps> = () => {
  const { playlist, status } = useAppSelector(
    (state) => ({
      playlist: state.rankingReducer.playlist,
      status: state.rankingReducer.currentStatus
    }),
    shallowEqual
  )

  return (
    <HeaderWrapper>
      <div className="inner">
        <LeftWrapper>
          <img src={getImageSize(playlist?.coverImgUrl, 150)} alt="" />
          <span className="sprite_cover cover"></span>
        </LeftWrapper>
        <RightWrapper>
          <h3 className="name">{playlist?.name}</h3>
          <div className="update">
            <i className="sprite_icon2 icon"></i>
            <span className="time">
              最近更新: {formatMonthDay(playlist?.updateTime)}
            </span>
            <span className="status">({status})</span>
          </div>
          <HeaderBar itemData={playlist} />
        </RightWrapper>
      </div>
    </HeaderWrapper>
  )
}

export default memo(RankingHeader)
