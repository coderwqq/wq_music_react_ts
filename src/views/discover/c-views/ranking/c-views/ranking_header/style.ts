import styled from 'styled-components'

export const HeaderWrapper = styled.div`
  padding: 40px;

  > .inner {
    display: flex;
    align-items: center;
  }
`
export const LeftWrapper = styled.div`
  float: left;
  position: relative;
  box-sizing: border-box;
  padding: 3px;
  width: 158px;
  height: 158px;
  border: 1px solid #ccc;

  .cover {
    position: absolute;
    left: 3px;
    top: 3px;
    width: 150px;
    height: 150px;
    background-position: -230px -380px;
  }
`
export const RightWrapper = styled.div`
  margin-left: 30px;

  .name {
    font-size: 20px;
    font-weight: 400;
    color: #333;
  }
  .update {
    display: flex;
    align-items: center;
    line-height: 35px;
    margin-bottom: 20px;

    .icon {
      display: inline-block;
      width: 13px;
      height: 13px;
      background-position: -18px -682px;
    }
    .time {
      font-size: 12px;
      color: #666;
      margin-left: 5px;
    }
    .status {
      margin-left: 10px;
      font-size: 12px;
      color: #999;
    }
  }
`
