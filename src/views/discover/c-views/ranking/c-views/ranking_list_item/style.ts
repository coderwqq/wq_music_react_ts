import styled from 'styled-components'

export const ListItemWrapper = styled.div`
  font-size: 12px;

  table {
    width: 100%;
    border: 1px solid #d9d9d9;
    border-collapse: collapse;
    border-spacing: 0;

    thead {
      tr {
        th {
          box-sizing: border-box;
          padding: 8px 10px;
          color: #666;
          text-align: left;
          font-weight: 400;
          height: 18px;
          line-height: 18px;
          border: 1px solid #ddd;
          border-width: 0 0 1px 1px;
        }
        .empty {
          border-left: none;
          width: 78px;
        }
        .title {
          width: 326px;
        }
        .duration {
          width: 89px;
        }
        .singer {
          width: 173px;
        }
      }
    }
    tbody {
      tr {
        &:hover {
          .duration {
            display: none;
          }
          .icon {
            display: flex;
          }
        }
        &:nth-child(2n + 1) {
          background-color: #f7f7f7;
        }
        &:nth-child(n-3) {
          height: 18px;
          line-height: 18px;
        }
        &:nth-child(-n + 3) {
          height: 50px;

          td {
            .content {
              padding: 4px 0;
            }
          }
          .song {
            display: flex;
            max-width: 192px !important;

            .name {
              ${(props) => props.theme.mixin.writeNowrap}
            }
          }
        }

        td {
          padding: 6px 10px;

          .number-icon {
            display: flex;

            .number {
              float: left;
              width: 25px;
              color: #999;
              text-align: center;
              margin-right: 12px;
            }
            .new {
              display: inline-block;
              width: 16px;
              height: 17px;
              background-position: -67px -283px;
            }
          }
          .image {
            margin-right: 14px;
          }
          .content {
            display: flex;
            align-items: center;

            .play {
              display: inline-block;
              margin-right: 8px;
              width: 17px;
              height: 17px;
              background-position: 0 -103px;

              &:hover {
                cursor: pointer;
                background-position: 0 -128px;
              }
            }
            .song {
              display: flex;
              max-width: 252px;
              ${(props) => props.theme.mixin.writeNowrap}

              .name {
                &:hover {
                  cursor: pointer;
                  text-decoration: underline;
                }
              }
              .alia {
                color: #aeaeae;
                ${(props) => props.theme.mixin.writeNowrap}

                .line {
                  padding: 0 3px;
                }
              }
            }
            .mv {
              display: inline-block;
              margin-left: 2px;
              width: 23px;
              height: 17px;
              background-position: 0 -151px;
            }
          }
          .duration {
            color: #666;
          }
          .icon {
            display: none;
            margin-right: -10px;

            .btn {
              display: inline-block;
              margin: 2px 0 0 4px;
              width: 18px;
              height: 16px;
              cursor: pointer;
            }
            .add {
              display: inline-block;
              margin-top: 2px;
              width: 13px;
              height: 13px;
              background-position: 0 -700px;

              &:hover {
                background-position: -22px -700px;
              }
            }
            .favor {
              background-position: 0 -174px;
              &:hover {
                background-position: -20px -174px;
              }
            }
            .share {
              background-position: 0 -195px;
              &:hover {
                background-position: -20px -195px;
              }
            }
            .download {
              background-position: -81px -174px;
              &:hover {
                background-position: -104px -174px;
              }
            }
          }
          .singer {
            width: 154px;
            ${(props) => props.theme.mixin.writeNowrap}

            .singer-name {
              &:hover {
                cursor: pointer;
                text-decoration: underline;
              }
            }
          }
        }
      }
    }
  }
`
