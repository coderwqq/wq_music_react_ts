import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { ListItemWrapper } from './style'
import { formatTime, getImageSize } from '@/utils/format'
import { fetchCurrentSongAction } from '@/store/modules/player'
import { useAppDispatch } from '@/store'

interface IProps {
  children?: ReactNode
  itemData: any
}

const RankingListItem: FC<IProps> = (props) => {
  const { itemData } = props

  const dispatch = useAppDispatch()
  function handlePlayClick(id: number) {
    dispatch(fetchCurrentSongAction(id))
  }

  function getSingerName(data: any) {
    const names: any[] = []
    data.map((item: any) => {
      names.push(item.name)
    })
    const name = names.join('/')
    return name
  }

  return (
    <ListItemWrapper>
      <table>
        <thead>
          <tr>
            <th className="sprite_table empty"></th>
            <th className="sprite_table title">标题</th>
            <th className="sprite_table duration">时长</th>
            <th className="sprite_table singer">歌手</th>
          </tr>
        </thead>
        <tbody>
          {itemData?.map((item: any, index: number) => {
            return (
              <tr key={item.id}>
                <td>
                  <div className="number-icon">
                    <span className="number">{index + 1}</span>
                    <span className="sprite_icon2 new"></span>
                  </div>
                </td>
                <td onClick={() => handlePlayClick(item.id)}>
                  <div className="content">
                    {(index === 0 || index === 1 || index === 2) && (
                      <img
                        src={getImageSize(item?.al?.picUrl, 50)}
                        className="image"
                      />
                    )}
                    <span
                      className="sprite_table play"
                      onClick={() => handlePlayClick(item.id)}
                    ></span>
                    <div className="song">
                      <span className="name">{item.name}</span>
                      {(!!item?.tns?.length || !!item?.alia?.length) && (
                        <div className="alia">
                          <span className="line">-</span>
                          <span className="alia-name">
                            ({item.tns || item.alia})
                          </span>
                        </div>
                      )}
                    </div>
                    {!!item.mv && <span className="sprite_table mv"></span>}
                  </div>
                </td>
                <td>
                  <span className="duration">{formatTime(item.dt)}</span>
                  <div className="icon">
                    <a className="sprite_icon2 add"></a>
                    <a className="sprite_table btn favor"></a>
                    <a className="sprite_table btn share"></a>
                    <a className="sprite_table btn download"></a>
                  </div>
                </td>
                <td>
                  <div className="singer">
                    <span className="singer-name">
                      {getSingerName(item?.ar)}
                    </span>
                  </div>
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </ListItemWrapper>
  )
}

export default memo(RankingListItem)
