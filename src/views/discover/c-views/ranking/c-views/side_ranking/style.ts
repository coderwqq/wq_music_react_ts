import styled from 'styled-components'

export const RankingListWrapper = styled.ul`
  .header {
    padding: 20px 10px 12px 15px;
    font-size: 15px;
    font-weight: 700;
  }
  .item {
    &:hover {
      background-color: #f4f2f2;
    }
  }
  .active {
    background-color: #e6e6e6;
    &:hover {
      background-color: #e6e6e6;
    }
  }
`
