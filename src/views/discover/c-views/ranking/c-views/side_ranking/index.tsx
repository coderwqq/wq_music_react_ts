import React, { memo, FC, useEffect } from 'react'
import type { ReactNode } from 'react'
import classNames from 'classnames'

import {
  changeCurrentIndexAction,
  changeCurrentStatusAction,
  fetchPlaylistDataAction
} from '@/store/modules/discover/ranking'
import { RankingListWrapper } from './style'
import { useAppDispatch, useAppSelector } from '@/store'
import SideItem from '../side_item'

interface IProps {
  children?: ReactNode
}

const SideRanking: FC<IProps> = () => {
  const { toplist, currentIndex } = useAppSelector((state) => ({
    toplist: state.rankingReducer?.toplist,
    currentIndex: state.rankingReducer?.currentIndex
  }))

  const dispatch = useAppDispatch()
  useEffect(() => {
    const id = toplist[currentIndex]?.id
    if (!id) return
    dispatch(fetchPlaylistDataAction(id))
  }, [toplist, currentIndex, dispatch])

  function handleItemListClick(index: number, status: string) {
    dispatch(changeCurrentIndexAction(index))
    dispatch(changeCurrentStatusAction(status))

    window.scroll(0, 0)
  }

  return (
    <RankingListWrapper>
      {toplist?.map((item: any, index: number) => {
        let header
        if (index === 0 || index === 4) {
          header = (
            <div className="header">
              {index === 0 ? '云音乐特色榜' : '全球媒体榜'}
            </div>
          )
        }
        return (
          <li key={item.id}>
            {header}
            <div
              className={classNames('item', { active: currentIndex === index })}
              onClick={() => handleItemListClick(index, item.updateFrequency)}
            >
              <SideItem itemData={item} />
            </div>
          </li>
        )
      })}
    </RankingListWrapper>
  )
}

export default memo(SideRanking)
