import React, { memo, FC, useEffect } from 'react'
import type { ReactNode } from 'react'

import { fetchToplistDataAction } from '@/store/modules/discover/ranking'
import { useAppDispatch } from '@/store'
import { RankingWrapper } from './style'
import SideRanking from './c-views/side_ranking'
import RankingHeader from './c-views/ranking_header'
import RankingList from './c-views/ranking_list'

interface IProps {
  children?: ReactNode
}

const Ranking: FC<IProps> = () => {
  const dispatch = useAppDispatch()
  useEffect(() => {
    dispatch(fetchToplistDataAction())
  }, [])

  return (
    <RankingWrapper>
      <div className="inner wrapV2">
        <div className="left">
          <SideRanking />
        </div>
        <div className="right">
          <RankingHeader />
          <RankingList />
        </div>
      </div>
    </RankingWrapper>
  )
}

export default memo(Ranking)
