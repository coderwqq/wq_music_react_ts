import styled from 'styled-components'

export const RankingWrapper = styled.div`
  > .inner {
    display: flex;
    min-height: 700px;
    background-color: #fff;
    border: 1px solid #d3d3d3;

    > .left {
      padding-top: 20px;
      width: 240px;
      background-color: #f9f9f9;
      border-right: 1px solid #d3d3d3;
    }
    > .right {
      width: 740px;
    }
  }
`
