import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { HeaderWrapper } from './style'

interface IProps {
  children?: ReactNode
  title: string
}

const SectionHeader: FC<IProps> = (props) => {
  const { title } = props

  return (
    <HeaderWrapper>
      <h3 className="title">{title}</h3>
      <a className="text">更多&gt;</a>
    </HeaderWrapper>
  )
}

export default memo(SectionHeader)
