import styled from 'styled-components'

export const HeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  height: 40px;
  border-bottom: 2px solid #c20c0c;

  > .title {
    font-size: 24px;
    font-weight: 400;
    margin-top: 3px;
  }
  > .text {
    font-size: 12px;
    color: #666;
    margin-top: 17px;

    &:hover {
      text-decoration: underline;
    }
  }
`
