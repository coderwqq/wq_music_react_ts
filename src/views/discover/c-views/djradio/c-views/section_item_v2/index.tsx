import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { ItemV2Wrapper } from './style'
import { getImageSize } from '@/utils/format'

interface IProps {
  children?: ReactNode
  itemData: any
}

const SectionItemV2: FC<IProps> = (props) => {
  const { itemData } = props

  return (
    <ItemV2Wrapper rank={itemData.rank} lastRank={itemData.lastRank}>
      <div className="left">
        <span className="rank">0{itemData.rank}</span>
        <div className="down">
          <i className="sprite_icon2 arrow"></i>
          {itemData.lastRank === -1 ? (
            ''
          ) : (
            <span className="number">
              {Math.abs(itemData.lastRank - itemData.rank)}
            </span>
          )}
        </div>
      </div>
      <a className="image">
        <img src={getImageSize(itemData?.program?.dj?.avatarUrl, 40)} />
        <i className="sprite_icon icon"></i>
      </a>
      <div className="info">
        <p className="name">{itemData?.program?.mainSong?.name}</p>
        <p className="radio-name">{itemData?.program?.dj?.brand}</p>
      </div>
    </ItemV2Wrapper>
  )
}

export default memo(SectionItemV2)
