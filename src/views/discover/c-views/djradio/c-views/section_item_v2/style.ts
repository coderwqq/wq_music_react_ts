import styled from 'styled-components'

interface IItemV2 {
  rank: number
  lastRank: number
}
export const ItemV2Wrapper = styled.div<IItemV2>`
  display: flex;
  align-items: center;
  padding: 10px 0;

  > .left {
    width: 47px;
    text-align: center;
    margin-top: 6px;

    .rank {
      color: ${(props) => (props.rank <= 3 ? '#da4545' : '#999')};
    }
    .down {
      font-size: 10px;

      .arrow {
        margin: 0 2px 1px 0;
        display: inline-block;
        width: ${(props) => (props.lastRank === -1 ? '16px' : '6px')};
        height: ${(props) => (props.lastRank === -1 ? '17px' : '6px')};
        background-position: ${(props) =>
          props.lastRank === props.rank
            ? '-74px -274px'
            : props.lastRank === -1
            ? '-67px -283px'
            : props.lastRank > props.rank
            ? '-74px -304px'
            : '-74px -324px'};
      }
      .number {
        color: ${(props) =>
          props.lastRank === props.rank
            ? '#999'
            : props.lastRank > props.rank
            ? '#ba2226'
            : '#4abbeb'};
      }
    }
  }
  > .image {
    position: relative;
    width: 40px;
    height: 40px;

    .icon {
      display: none;
      position: absolute;
      top: 50%;
      left: 50%;
      width: 22px;
      height: 22px;
      margin: -11px 0 0 -11px;
      background-position: 0 -85px;
    }
  }
  > .info {
    font-size: 12px;
    line-height: 20px;
    width: 208px;
    margin-left: 10px;

    .name {
      ${(props) => props.theme.mixin.writeNowrap}

      &:hover {
        text-decoration: underline;
        cursor: pointer;
      }
    }
    .radio-name {
      color: #999;
      ${(props) => props.theme.mixin.writeNowrap}

      &:hover {
        text-decoration: underline;
        cursor: pointer;
      }
    }
  }
`
