import styled from 'styled-components'

interface ICategory {
  currentIndex: number
}
export const CategoryWrapper = styled.div<ICategory>`
  position: relative;
  margin-left: -33px;

  .arrow {
    position: absolute;
    width: 20px;
    height: 30px;
    top: 50%;
    transform: translateY(-50%);
    opacity: 0.25;
  }
  .arrow-left {
    left: 8px;
    background-position: 0 -30px;
    opacity: ${(props) => (props.currentIndex === 0 ? 0.08 : 0.25)};

    &:hover {
      opacity: ${(props) => (props.currentIndex === 0 ? 0.08 : 0.4)};
      cursor: pointer;
    }
  }
  .arrow-right {
    right: -25px;
    background-position: -30px -30px;
    opacity: ${(props) => (props.currentIndex === 1 ? 0.08 : 0.25)};

    &:hover {
      opacity: ${(props) => (props.currentIndex === 1 ? 0.08 : 0.4)};
      cursor: pointer;
    }
  }

  .category-page {
    display: block;

    .category-item {
      float: left;
      margin: 0 0 25px 33px;

      a {
        display: block;
        width: 70px;
        height: 70px;
        text-align: center;

        &:hover {
          background-color: #f6f6f6;
          border-radius: 5px;
        }
        .image {
          margin: 2px auto 0;
        }
        .text {
          font-size: 12px;
          color: #888;
          margin-top: -3px;
        }
      }
    }
  }
  > ul {
    position: absolute;
    display: flex;
    justify-content: center;
    left: 33px;
    bottom: 3px;
    width: 900px;
    height: 20px;

    li {
      .item {
        display: inline-block;
        width: 6px;
        height: 6px;
        margin: 0 10px;
        background-color: #ddd;
        border-radius: 50%;
      }
      .active {
        background-color: #ba2300;
      }
    }
  }
`
interface IImage {
  imgUrl: string
}
export const ImageWrapper = styled.div<IImage>`
  width: 48px;
  height: 48px;
  background-image: url(${(props) => props.imgUrl});
`
