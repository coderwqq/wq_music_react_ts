import React, { memo, FC, useRef, ElementRef, useState } from 'react'
import type { ReactNode } from 'react'
import { Carousel } from 'antd'
import { shallowEqual } from 'react-redux'

import { CategoryWrapper, ImageWrapper } from './style'
import { useAppSelector } from '@/store'
import classNames from 'classnames'

interface IProps {
  children?: ReactNode
}

const RadioCategory: FC<IProps> = () => {
  const radioRef = useRef<ElementRef<typeof Carousel>>(null)
  const [currentIndex, setCurrentIndex] = useState(0)
  const { radioCategory } = useAppSelector(
    (state) => ({
      radioCategory: state.djradioReducer.radioCategory
    }),
    shallowEqual
  )

  const PAGE_SIZE = 18
  const page = Math.ceil(radioCategory.length / PAGE_SIZE || 1)

  function getSize(index: number) {
    return index * PAGE_SIZE
  }

  function handleArrowClick(isNext = true) {
    if (isNext) {
      if (currentIndex === 0) {
        radioRef.current?.next()
        setCurrentIndex(currentIndex + 1)
      }
    } else {
      if (currentIndex === 1) {
        radioRef.current?.prev()
        setCurrentIndex(currentIndex - 1)
      }
    }
  }

  return (
    <CategoryWrapper currentIndex={currentIndex}>
      <Carousel ref={radioRef} dots={false}>
        {Array(page)
          .fill(0)
          .map((_, index) => {
            return (
              <ul key={index} className="category-page">
                {radioCategory
                  .slice(index * PAGE_SIZE, getSize(index + 1))
                  .map((item: any) => {
                    return (
                      <li key={item.id} className="category-item">
                        <a>
                          <ImageWrapper
                            className="image"
                            imgUrl={item.picWebUrl}
                          ></ImageWrapper>
                          <span className="text">{item.name}</span>
                        </a>
                      </li>
                    )
                  })}
              </ul>
            )
          })}
      </Carousel>
      <ul>
        {Array(page)
          .fill(0)
          .map((_, index) => {
            return (
              <li key={index}>
                <span
                  className={classNames('item', {
                    active: index === currentIndex
                  })}
                ></span>
              </li>
            )
          })}
      </ul>
      <div
        className="radio_slide arrow arrow-left"
        onClick={() => handleArrowClick(false)}
      ></div>
      <div
        className="radio_slide arrow arrow-right"
        onClick={() => handleArrowClick(true)}
      ></div>
    </CategoryWrapper>
  )
}

export default memo(RadioCategory)
