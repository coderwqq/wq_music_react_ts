import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { ItemV1Wrapper } from './style'
import { getImageSize } from '@/utils/format'

interface IProps {
  children?: ReactNode
  itemData: any
}

const SectionItemV1: FC<IProps> = (props) => {
  const { itemData } = props

  return (
    <ItemV1Wrapper>
      <a className="image">
        <img src={getImageSize(itemData.coverUrl, 40)} />
        <i className="sprite_icon icon"></i>
      </a>
      <div className="info">
        <p className="name">{itemData.name}</p>
        <p className="radio-name">{itemData.radio?.name}</p>
      </div>
      <a className="category">{itemData?.radio?.category}</a>
    </ItemV1Wrapper>
  )
}

export default memo(SectionItemV1)
