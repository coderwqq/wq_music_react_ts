import styled from 'styled-components'

export const ItemV1Wrapper = styled.div`
  display: flex;
  align-items: center;
  padding: 10px 0;

  &:hover {
    background-color: #eee;
    .image {
      .icon {
        display: block;
      }
    }
  }

  > .image {
    position: relative;
    margin-left: 20px;
    width: 40px;
    height: 40px;

    .icon {
      display: none;
      position: absolute;
      top: 50%;
      left: 50%;
      width: 22px;
      height: 22px;
      margin: -11px 0 0 -11px;
      background-position: 0 -85px;
    }
  }
  > .info {
    font-size: 12px;
    line-height: 20px;
    width: 254px;
    margin-left: 10px;

    .name {
      ${(props) => props.theme.mixin.writeNowrap}

      &:hover {
        text-decoration: underline;
        cursor: pointer;
      }
    }
    .radio-name {
      color: #999;
      ${(props) => props.theme.mixin.writeNowrap}

      &:hover {
        text-decoration: underline;
        cursor: pointer;
      }
    }
  }
  .category {
    font-size: 12px;
    color: #999;
    padding: 1px 6px 0;
    margin-left: 10px;
    border: 1px solid #999;

    &:hover {
      color: #666;
      border-color: #666;
    }
  }
`
