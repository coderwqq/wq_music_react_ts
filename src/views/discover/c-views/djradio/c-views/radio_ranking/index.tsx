import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { shallowEqual } from 'react-redux'
import { useAppSelector } from '@/store'
import { RankingWrapper } from './style'
import SectionHeader from '../section_header'
import SectionItemV1 from '../section_item_v1'
import SectionItemV2 from '../section_item_v2'

interface IProps {
  children?: ReactNode
}

const RadioRanking: FC<IProps> = () => {
  const { recommendProgram, programRanking } = useAppSelector(
    (state) => ({
      recommendProgram: state.djradioReducer.recommendProgram,
      programRanking: state.djradioReducer.programRanking
    }),
    shallowEqual
  )

  return (
    <RankingWrapper>
      <div className="left">
        <SectionHeader title="推荐节目" />
        <ul>
          {recommendProgram.map((item: any) => {
            return (
              <li key={item.id}>
                <SectionItemV1 itemData={item} />
              </li>
            )
          })}
        </ul>
      </div>
      <div className="right">
        <SectionHeader title="节目排行榜" />
        <ul>
          {programRanking.map((item: any) => {
            return (
              <li key={item.program.mainSong.id}>
                <SectionItemV2 itemData={item} />
              </li>
            )
          })}
        </ul>
      </div>
    </RankingWrapper>
  )
}

export default memo(RadioRanking)
