import styled from 'styled-components'

export const RankingWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 10px;

  > .left,
  > .right {
    width: 426px;

    ul {
      border: 1px solid #e2e2e2;
      border-width: 0 1px 1px;

      li {
        &:nth-child(2n) {
          background-color: #f7f7f7;
        }
      }
    }
  }
`
