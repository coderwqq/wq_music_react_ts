import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { XXRadioWrapper } from './style'
import SectionHeader from '../section_header'
import { getImageSize } from '@/utils/format'

interface IProps {
  children?: ReactNode
  title: string
  itemData?: any
}

const XXRadio: FC<IProps> = (props) => {
  const { title, itemData } = props

  return (
    <XXRadioWrapper>
      <SectionHeader title={title} />
      <ul>
        {itemData?.slice(0, 4).map((item: any) => {
          return (
            <li key={item.id}>
              <img src={getImageSize(item.picUrl, 120)} />
              <p className="right">
                <h3 className="name">{item.name}</h3>
                <span className="rcmdtext">{item.rcmdtext}</span>
              </p>
            </li>
          )
        })}
      </ul>
    </XXRadioWrapper>
  )
}

export default memo(XXRadio)
