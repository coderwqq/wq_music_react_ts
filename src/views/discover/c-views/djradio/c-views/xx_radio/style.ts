import styled from 'styled-components'

export const XXRadioWrapper = styled.div`
  margin-top: 35px;

  > ul {
    display: flex;
    flex-wrap: wrap;
    margin-left: -30px;

    li {
      display: flex;
      width: 435px;
      margin-left: 30px;
      padding: 20px 0;

      img {
        &:hover {
          cursor: pointer;
        }
      }
      .right {
        margin-left: 20px;

        .name {
          font-size: 18px;
          margin: 16px 0 20px;

          &:hover {
            color: #000;
            text-decoration: underline;
            cursor: pointer;
          }
        }
        .rcmdtext {
          font-size: 12px;
          color: #999;
        }
      }
    }
  }
`
