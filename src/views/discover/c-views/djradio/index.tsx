import React, { memo, FC, useEffect } from 'react'
import type { ReactNode } from 'react'

import { useAppDispatch, useAppSelector } from '@/store'
import { fetchDjradioDataAction } from '@/store/modules/discover/djradio'
import { RadioWrapper } from './style'
import RadioCategory from './c-views/radio_category'
import RadioRanking from './c-views/radio_ranking'
import XXRadio from './c-views/xx_radio'

interface IProps {
  children?: ReactNode
}

const Djradio: FC<IProps> = () => {
  const dispatch = useAppDispatch()
  useEffect(() => {
    dispatch(fetchDjradioDataAction())
  }, [])

  const { djCover } = useAppSelector((state) => ({
    djCover: state.djradioReducer.djCover
  }))

  return (
    <RadioWrapper>
      <RadioCategory />
      <RadioRanking />
      {/* <XXRadio title="音乐播客·电台" /> */}
      {/* <XXRadio title="生活·电台" /> */}
      {/* <XXRadio title="情感·电台" /> */}
      <XXRadio title="创作翻唱·电台" itemData={djCover} />
      {/* <XXRadio title="知识·电台" /> */}
    </RadioWrapper>
  )
}

export default memo(Djradio)
