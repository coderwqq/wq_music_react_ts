import styled from 'styled-components'

export const RadioWrapper = styled.div`
  box-sizing: border-box;
  padding: 40px;
  margin: 0 auto;
  width: 982px;
  background-color: #fff;
  border: 1px solid #d3d3d3;
  border-width: 0 1px;
`
