import styled from 'styled-components'

export const AlbumWrapper = styled.div`
  box-sizing: border-box;
  margin: 0 auto;
  padding: 40px;
  width: 982px;
  background-color: #fff;
  border: 1px solid #d3d3d3;
  border-width: 0 1px;
`
