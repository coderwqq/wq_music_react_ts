import React, { memo, FC } from 'react'
import { shallowEqual } from 'react-redux'
import type { ReactNode } from 'react'

import { useAppSelector } from '@/store'
import { AllWrapper } from './style'
import SectionHeader from '../section_header'
import AlbumItem from '../album_item'

interface IProps {
  children?: ReactNode
}

const AllAlbum: FC<IProps> = () => {
  const { allAlbum } = useAppSelector(
    (state) => ({
      allAlbum: state.albumReducer.allAlbum
    }),
    shallowEqual
  )
  console.log(allAlbum)

  return (
    <AllWrapper>
      <SectionHeader title="全部新碟" />
      <ul>
        {allAlbum?.map((item: any) => {
          return (
            <li key={item.id}>
              <AlbumItem itemData={item} />
            </li>
          )
        })}
      </ul>
    </AllWrapper>
  )
}

export default memo(AllAlbum)
