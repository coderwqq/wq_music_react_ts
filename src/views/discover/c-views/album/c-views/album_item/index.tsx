import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { ItemWrapper } from './style'
import { getImageSize } from '@/utils/format'

interface IProps {
  children?: ReactNode
  itemData: any
}

const AlbumItem: FC<IProps> = (props) => {
  const { itemData } = props

  return (
    <ItemWrapper>
      <div className="top">
        <img src={getImageSize(itemData.picUrl, 130)} />
        <a className="sprite_cover cover"></a>
        <a className="sprite_icon icon"></a>
      </div>
      <p className="song-name">{itemData.name}</p>
      {itemData?.artists.length === 1 ? (
        <p className="artist-name-v1">{itemData.artist.name}</p>
      ) : (
        <div className="artists">
          <p>
            {itemData.artists.map((item: any) => {
              return (
                <span className="artist-name-v2" key={item.id}>
                  <a>{item.name}</a>
                  <a className="line">/</a>
                </span>
              )
            })}
          </p>
        </div>
      )}
    </ItemWrapper>
  )
}

export default memo(AlbumItem)
