import styled from 'styled-components'

export const ItemWrapper = styled.div`
  .top {
    position: relative;

    .cover {
      position: absolute;
      top: 0;
      left: 0;
      width: 153px;
      height: 130px;
      background-position: 0 -845px;
    }
    .icon {
      display: none;
      position: absolute;
      right: 30px;
      bottom: 8px;
      width: 28px;
      height: 28px;
      background-position: 0 -140px;
    }
    &:hover {
      .icon {
        display: block !important;

        &:hover {
          background-position: 0 -170px;
        }
      }
    }
  }
  .song-name {
    color: #000;
    ${(props) => props.theme.mixin.writeNowrap}
    margin: 8px 0 3px;
    line-height: 1.4;

    &:hover {
      text-decoration: underline;
      cursor: pointer;
    }
  }
  .artist-name-v1 {
    font-size: 12px;
    color: #666;
    line-height: 1.4;
    ${(props) => props.theme.mixin.writeNowrap}

    &:hover {
      text-decoration: underline;
      cursor: pointer;
    }
  }
  .artists {
    display: flex;
    max-width: 85%;

    p {
      ${(props) => props.theme.mixin.writeNowrap}

      &:hover {
        text-decoration: underline;
        cursor: pointer;
      }
      .artist-name-v2 {
        a {
          font-size: 12px;
          color: #666;
        }
        .line {
          padding: 0 6px;
        }
      }
    }
  }
`
