import styled from 'styled-components'

export const HeaderWrapper = styled.div`
  display: flex;
  height: 40px;
  border-bottom: 2px solid #c20c0c;

  .title {
    font-size: 24px;
    font-weight: 400;
    margin-top: 3px;
  }
`
