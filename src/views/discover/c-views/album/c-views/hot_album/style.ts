import styled from 'styled-components'

export const HotWrapper = styled.div`
  ul {
    display: flex;
    flex-wrap: wrap;
    margin: 20px 0 0 -33px;

    li {
      width: 153px;
      height: 178px;
      padding: 0 0 30px 33px;
    }
  }
`
