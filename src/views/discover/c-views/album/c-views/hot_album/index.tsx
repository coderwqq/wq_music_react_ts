import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'
import { shallowEqual } from 'react-redux'

import { useAppSelector } from '@/store'
import { HotWrapper } from './style'
import SectionHeader from '../section_header'
import AlbumItem from '../album_item'

interface IProps {
  children?: ReactNode
}

const HotAlbum: FC<IProps> = () => {
  const { hotAlbum } = useAppSelector(
    (state) => ({
      hotAlbum: state.albumReducer.hotAlbum
    }),
    shallowEqual
  )

  return (
    <HotWrapper>
      <SectionHeader title="热门新碟" />
      <ul>
        {hotAlbum.slice(0, 10).map((item: any) => {
          return (
            <li key={item.id}>
              <AlbumItem itemData={item} />
            </li>
          )
        })}
      </ul>
    </HotWrapper>
  )
}

export default memo(HotAlbum)
