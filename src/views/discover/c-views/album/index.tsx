import React, { memo, FC, useEffect } from 'react'
import type { ReactNode } from 'react'

import { useAppDispatch } from '@/store'
import { AlbumWrapper } from './style'
import { fetchAlbumDataAction } from '@/store/modules/discover/album'
import HotAlbum from './c-views/hot_album'
import AllAlbum from './c-views/all_album'

interface IProps {
  children?: ReactNode
}
const Album: FC<IProps> = () => {
  const dispatch = useAppDispatch()
  useEffect(() => {
    dispatch(fetchAlbumDataAction())
  }, [])

  return (
    <AlbumWrapper>
      <HotAlbum />
      <AllAlbum />
    </AlbumWrapper>
  )
}

export default memo(Album)
