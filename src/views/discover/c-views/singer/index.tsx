import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { SingerWrapper } from './style'
import SideCategory from './c-views/side_category'

interface IProps {
  children?: ReactNode
}

const Singer: FC<IProps> = () => {
  return (
    <SingerWrapper>
      <div className="left">
        <SideCategory />
      </div>
      <div className="right">right</div>
    </SingerWrapper>
  )
}

export default memo(Singer)
