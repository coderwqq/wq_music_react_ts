import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'
import classNames from 'classnames'

import { useAppDispatch, useAppSelector } from '@/store'
import { CategoryWrapper } from './style'
import { artistCategories } from '@/assets/data/local_data'
import {
  changeCurrentAreaAction,
  changeCurrentTypeAction
} from '@/store/modules/discover/singer'

interface IProps {
  children?: ReactNode
}

const SideCategory: FC<IProps> = () => {
  const { currentArea, currentType } = useAppSelector((state) => ({
    currentArea: state.singerReducer.currentArea,
    currentType: state.singerReducer.currentType
  }))

  const dispatch = useAppDispatch()
  function selectArtist(area: number, type: number) {
    dispatch(changeCurrentAreaAction(area))
    dispatch(changeCurrentTypeAction(type))
  }

  return (
    <CategoryWrapper>
      {artistCategories.map((item: any) => {
        return (
          <div key={item.area} className="category">
            <h3 className="title">{item.title}</h3>
            <ul>
              {item.artists.map((iten: any) => {
                const isSelect =
                  currentArea === item.area && currentType.type === iten.type
                return (
                  <li
                    key={iten.type}
                    className={classNames({ active: isSelect })}
                    onClick={() => selectArtist(item.area, iten.type)}
                  >
                    <i className="singer_sprite"></i>
                    <span className="name">{iten.name}</span>
                  </li>
                )
              })}
            </ul>
          </div>
        )
      })}
    </CategoryWrapper>
  )
}

export default memo(SideCategory)
