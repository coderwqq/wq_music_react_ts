import styled from 'styled-components'

export const CategoryWrapper = styled.div`
  margin-top: 51px;
  padding: 0 10px 40px;

  > .category {
    &:nth-child(n + 2) {
      margin-top: 5px;
      padding-top: 16px;
      border-top: 1px solid #d3d3d3;
    }

    .title {
      font-size: 16px;
      padding-left: 14px;
      height: 25px;
      line-height: 25px;
    }
    > ul {
      li {
        position: relative;
        width: 160px;
        height: 29px;
        margin-bottom: 2px;

        i {
          position: absolute;
          width: 133px;
          height: 29px;
          background-position: 0 -30px;
        }
        .name {
          position: relative;
          display: block;
          font-size: 12px;
          padding-left: 27px;
          height: 29px;
          line-height: 29px;

          &:hover {
            text-decoration: underline;
            cursor: pointer;
          }
        }
      }
      .active {
        color: #c20c0c;
        i {
          width: 160px;
          background-position: 0 0;
        }
        .name {
          &:hover {
            text-decoration: none;
          }
        }
      }
    }
  }
`
