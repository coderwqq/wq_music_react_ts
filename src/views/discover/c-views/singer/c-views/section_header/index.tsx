import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { HeaderWrapper } from './style'

interface IProps {
  children?: ReactNode
}

const SectionHeader: FC<IProps> = () => {
  return <HeaderWrapper>SectionHeader</HeaderWrapper>
}

export default memo(SectionHeader)
