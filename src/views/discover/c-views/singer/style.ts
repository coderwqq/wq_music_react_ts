import styled from 'styled-components'

export const SingerWrapper = styled.div`
  display: flex;
  box-sizing: border-box;
  margin: 0 auto;
  width: 982px;
  background-color: #fff;
  border: 1px solid #d3d3d3;
  border-width: 0 1px;

  > .left {
    width: 180px;
    background-color: #f9f9f9;
    border-bottom: 1px solid #d3d3d3;
  }
  > .right {
    box-sizing: border-box;
    padding: 40px;
    width: 800px;
    border-left: 1px solid #d3d3d3;
  }
`
