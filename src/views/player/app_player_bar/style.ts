import styled from 'styled-components'

export const PlayerBarWrapper = styled.div`
  position: fixed;
  left: 0;
  right: 0;
  bottom: 0;
  height: 53px;
  background-position: 0 0;
  background-repeat: repeat-x;

  > .inner {
    display: flex;
    align-items: center;
    width: 980px;
    height: 47px;
    margin: 6px auto 0;
  }
`
interface IBarLeft {
  isPlaying: boolean
}
export const BarLeftWrapper = styled.div<IBarLeft>`
  display: flex;
  align-items: center;
  width: 137px;

  .btn {
    display: inline-block;
    margin-right: 8px;
  }
  .prev {
    width: 28px;
    height: 28px;
    background-position: 0 -130px;
  }
  .play {
    width: 36px;
    height: 36px;
    background-position: 0 ${(props) => (props.isPlaying ? '-165px' : '-204px')};
  }
  .next {
    width: 28px;
    height: 28px;
    background-position: -80px -130px;
  }
`
export const BarCenterWrapper = styled.div`
  display: flex;
  align-items: center;
  width: 642px;

  .image {
    position: relative;
    display: flex;
    align-items: center;
    margin-right: 15px;

    .cover {
      position: absolute;
      display: inline-block;
      left: 0;
      top: 0;
      width: 34px;
      height: 34px;
      background-position: 0 -80px;
    }
  }
  .info {
    .song {
      position: relative;
      top: 7px;
      font-size: 12px;

      .song-name {
        max-width: 300px;
        color: #e8e8e8;
        ${(props) => props.theme.mixin.writeNowrap}
      }
      .singer-name {
        margin-left: 15px;
        max-width: 220px;
        color: #9b9b9b;
        ${(props) => props.theme.mixin.writeNowrap}
      }
    }
    .progress {
      display: flex;
      align-items: center;

      .ant-slider {
        width: 466px;
        margin-left: 0;
        margin-right: 10px;

        .ant-slider-rail {
          height: 9px;
          background: url(${require('@/assets/img/progress_bar.png')}) right 0;
        }
        .ant-slider-track {
          height: 9px;
          background: url(${require('@/assets/img/progress_bar.png')}) left -66px;
        }
        .ant-slider-handle {
          width: 22px;
          height: 24px;
          border: none;
          margin-top: -4px;
          background: url(${require('@/assets/img/sprite_icon.png')}) 0 -250px;

          &::before,
          &::after {
            content: none;
          }
        }
      }
      .time {
        font-size: 12px;

        .current-time {
          color: #a1a1a1;
        }
        .line {
          color: #797979;
          margin: 0 2px;
        }
        .total-time {
          color: #797979;
        }
      }
    }
  }
`
interface IPlayMode {
  playMode: number
}
export const BarRightWrapper = styled.div<IPlayMode>`
  display: flex;
  align-items: center;
  margin-top: 3px;

  .btn {
    display: inline-block;
    width: 25px;
    height: 25px;
    margin-right: 2px;
  }
  > .left {
    width: 87px;

    .pip {
      background: url(${require('@/assets/img/pip_icon')});
    }
    .favor {
      background-position: -88px -163px;
    }
    .share {
      background-position: -114px -163px;
    }
  }
  > .right {
    width: 113px;
    padding-left: 13px;
    background-position: -147px -248px;

    .volume {
      background-position: -2px -248px;
    }
    .loop {
      background-position: ${(props) => {
        switch (props.playMode) {
          case 1:
            return '-66px -248px'
          case 2:
            return '-66px -344px'
          default:
            return '-3px -344px'
        }
      }};
    }
    .playlist {
      display: inline-block;
      width: 59px;
      height: 25px;
      background-position: -42px -68px;
    }
  }
`
