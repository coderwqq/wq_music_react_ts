import React, { memo, FC, useRef, useEffect, useState } from 'react'
import type { ReactNode } from 'react'
import { shallowEqual } from 'react-redux'
import { Slider, message } from 'antd'

import {
  BarCenterWrapper,
  BarLeftWrapper,
  BarRightWrapper,
  PlayerBarWrapper
} from './style'
import { useAppDispatch, useAppSelector } from '@/store'
import { getImageSize, formatTime } from '@/utils/format'
import { getSongPlayUrl } from '@/utils/handle_player'
import {
  changeLyricIndexAction,
  changePlayModeAction,
  toggleSongAction
} from '@/store/modules/player'

interface IProps {
  children?: ReactNode
}

const AppPlayerBar: FC<IProps> = () => {
  const [isPlaying, setIsPlaying] = useState(false)
  const [duration, setDuration] = useState(0)
  const [progress, setProgress] = useState(0)
  const [currentTime, setCurrentTime] = useState(0)
  const [isSliding, setIsSliding] = useState(false)
  const audioRef = useRef<HTMLAudioElement>(null)

  const dispach = useAppDispatch()
  const { currentSong, lyrics, lyricIndex, playMode } = useAppSelector(
    (state) => ({
      currentSong: state.playerReducer.currentSong,
      lyrics: state.playerReducer.lyrics,
      lyricIndex: state.playerReducer.lyricIndex,
      playMode: state.playerReducer.playMode
    }),
    shallowEqual
  )

  useEffect(() => {
    // 获取播放歌曲的url
    audioRef.current!.src = getSongPlayUrl(currentSong.id)
    audioRef.current
      ?.play()
      .then(() => {
        setIsPlaying(true)
        // console.log('歌曲播放成功')
      })
      .catch((err) => {
        setIsPlaying(false)
        console.log('歌曲播放失败', err)
      })

    setDuration(currentSong.dt)
  }, [currentSong])

  function handlePlayBtnClick() {
    isPlaying ? audioRef.current?.pause() : audioRef.current?.play()
    setIsPlaying(!isPlaying)
  }

  function handleToggleSongClick(isNext = true) {
    dispach(toggleSongAction(isNext))
  }

  // 音乐播放进度处理
  function handleTimeUpdate() {
    const currentTime = audioRef.current!.currentTime * 1000
    if (!isSliding) {
      const progress = (currentTime / duration) * 100
      setProgress(progress)
      setCurrentTime(currentTime)
    }

    // 根据当前时间匹配对应歌词
    let index = lyrics.length - 1
    for (let i = 0; i < lyrics.length; i++) {
      const lyric = lyrics[i]
      if (lyric.time > currentTime) {
        index = i - 1
        break
      }
    }

    // 防止歌词多次渲染的处理
    if (lyricIndex === index || index === -1) return
    dispach(changeLyricIndexAction(index))

    // 界面上歌词的显示
    message.open({
      content: lyrics[index].text,
      key: 'lyric',
      duration: 0
    })
  }

  function handleTimeEnded() {
    if (playMode === 2) {
      audioRef.current!.currentTime = 0
      audioRef.current!.play()
    } else {
      handleToggleSongClick(true)
    }
  }

  // 滑动播放条的处理
  function handleSliderChange(value: number) {
    setIsSliding(true)

    const currentTime = (value / 100) * duration
    setCurrentTime(currentTime)
    setProgress(value)
  }

  // 鼠标放开时 播放条的处理
  function handleSliderAfterChange(value: number) {
    const currentTime = (value / 100) * duration
    audioRef.current!.currentTime = currentTime / 1000
    setCurrentTime(currentTime)
    setProgress(value)
    setIsSliding(false)
  }

  function handlePlayModeClick() {
    let newPlayMode = playMode + 1
    if (newPlayMode > 2) newPlayMode = 0
    dispach(changePlayModeAction(newPlayMode))
  }

  const singerName: string[] = []
  currentSong?.ar?.map((item: any) => {
    singerName.push(item.name)
  })

  return (
    <PlayerBarWrapper className="sprite_playbar">
      <div className="inner">
        <BarLeftWrapper isPlaying={isPlaying}>
          <a
            className="sprite_playbar btn prev"
            onClick={() => handleToggleSongClick(false)}
          ></a>
          <a
            className="sprite_playbar btn play"
            onClick={handlePlayBtnClick}
          ></a>
          <a
            className="sprite_playbar btn next"
            onClick={() => handleToggleSongClick(true)}
          ></a>
        </BarLeftWrapper>
        <BarCenterWrapper>
          <div className="image">
            <img src={getImageSize(currentSong?.al?.picUrl, 34)} alt="" />
            <a className="sprite_playbar cover"></a>
          </div>
          <div className="info">
            <div className="song">
              <span className="song-name">{currentSong.name}</span>
              <span className="singer-name">{singerName.join('/')}</span>
            </div>
            <div className="progress">
              <Slider
                step={0.5}
                value={progress}
                tooltip={{ formatter: null }}
                onChange={handleSliderChange}
                onAfterChange={handleSliderAfterChange}
              />
              <div className="time">
                <span className="current-time">{formatTime(currentTime)}</span>
                <span className="line">/</span>
                <span className="total-time">{formatTime(duration)}</span>
              </div>
            </div>
          </div>
        </BarCenterWrapper>
        <BarRightWrapper playMode={playMode}>
          <div className="left">
            <a className="btn pip"></a>
            <a className="btn sprite_playbar favor"></a>
            <a className="btn sprite_playbar share"></a>
          </div>
          <div className="right sprite_playbar">
            <a className="btn sprite_playbar volume"></a>
            <a
              className="btn sprite_playbar loop"
              onClick={handlePlayModeClick}
            ></a>
            <a className="sprite_playbar playlist"></a>
          </div>
        </BarRightWrapper>
      </div>
      <audio
        ref={audioRef}
        onTimeUpdate={handleTimeUpdate}
        onEnded={handleTimeEnded}
      />
    </PlayerBarWrapper>
  )
}

export default memo(AppPlayerBar)
