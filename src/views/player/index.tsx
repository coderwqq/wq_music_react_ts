import React, { memo, FC } from 'react'
import type { ReactNode } from 'react'

import { PlayerWrapper } from './style'
import AppPlayerBar from './app_player_bar'

interface IProps {
  children?: ReactNode
}

const Player: FC<IProps> = () => {
  return (
    <PlayerWrapper>
      <AppPlayerBar />
    </PlayerWrapper>
  )
}

export default memo(Player)
