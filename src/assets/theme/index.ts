export const theme = {
  color: {
    primary: '#c20c0c'
  },
  mixin: {
    writeNowrap: `
      white-space: nowrap;
      text-overflow: ellipsis;
      overflow: hidden;
    `
  }
}
