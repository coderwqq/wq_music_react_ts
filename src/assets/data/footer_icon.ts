export const footerIcon = [
  {
    text: '音乐开放平台',
    background: '-170px -5px',
    bg_hover: '-5px -115px',
    link: 'https://developer.music.163.com/st/developer'
  },
  {
    text: '云村交易所',
    background: '-5px -170px',
    bg_hover: '-60px -170px',
    link: 'https://music.163.com/st/web-sublicense/home'
  },
  {
    text: 'Amped Studio',
    background: '-5px -60px',
    bg_hover: '-60px -5px',
    link: 'https://web-amped.music.163.com/'
  },
  {
    text: '用户认证',
    background: '-60px -60px',
    bg_hover: '-115px -5px',
    link: 'https://music.163.com/st/userbasic#/auth'
  },
  {
    text: '音乐交易平台',
    background: '-115px -115px',
    bg_hover: '-5px -5px',
    link: 'https://music.163.com/st/ad-cms-bills/mlogin?from=mainStation'
  },
  {
    text: '赞赏',
    background: '-170px -115px',
    bg_hover: '-60px -115px',
    link: 'https://music.163.com/web/reward'
  },
  {
    text: '视频激励',
    background: '-170px -60px',
    bg_hover: '-115px -60px',
    link: 'https://music.163.com/st/ncreator/revenue-plan'
  }
]
