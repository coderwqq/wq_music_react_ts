export function formatCount(count: number) {
  if (count > 100000) {
    return Math.floor(count / 10000) + '万'
  }
  return count
}

export function getImageSize(
  imageUrl: string,
  width: number,
  height: number = width
) {
  return imageUrl + `?param=${width}y${height}`
}

export function formatTime(time: number) {
  const currentTime = time / 1000

  // Math.floor()向下取整
  const minute = Math.floor(currentTime / 60)
  const seconds = Math.floor(currentTime) % 60 // % 取余

  const minuteFormat = String(minute).padStart(2, '0')
  const secondsFormat = String(seconds).padStart(2, '0')
  return `${minuteFormat}:${secondsFormat}`
}

export function formatDate(time: number, fmt: string) {
  const date = new Date(time)

  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(
      RegExp.$1,
      (date.getFullYear() + '').substr(4 - RegExp.$1.length)
    )
  }
  const o: any = {
    'M+': date.getMonth() + 1,
    'd+': date.getDate(),
    'h+': date.getHours(),
    'm+': date.getMinutes(),
    's+': date.getSeconds()
  }
  for (const k in o) {
    if (new RegExp(`(${k})`).test(fmt)) {
      const str = o[k] + ''
      fmt = fmt.replace(
        RegExp.$1,
        RegExp.$1.length === 1 ? str : padLeftZero(str)
      )
    }
  }
  return fmt
}

function padLeftZero(str: string) {
  return ('00' + str).substr(str.length)
}

export function formatMonthDay(time: number) {
  return formatDate(time, 'MM月dd日')
}
