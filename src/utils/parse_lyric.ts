export interface Ilyric {
  time: number
  text: string
}

const timeRegExp = /\[(\d{2}):(\d{2})\.(\d{2,3})\]/
const signRegExp = /，$|。$/gi
export function parseLyric(lyricString: string) {
  const lyrics: Ilyric[] = []

  const lines = lyricString.split('\n')
  for (const line of lines) {
    const results = timeRegExp.exec(line) //获取每行歌词的时间
    if (!results) continue

    const time1 = Number(results[1]) * 60 * 1000
    const time2 = Number(results[2]) * 1000
    const time3 =
      results[3].length === 2 ? Number(results[3]) * 10 : Number(results[3])
    const time = time1 + time2 + time3
    const text = line.replace(timeRegExp, '').replace(signRegExp, '') //获取每行歌词

    lyrics.push({ time, text })
  }
  return lyrics
}
