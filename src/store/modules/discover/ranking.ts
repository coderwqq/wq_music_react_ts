import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import {
  getToplistData,
  getPlaylistData
} from '@/service/modules/discover/ranking'

export const fetchToplistDataAction = createAsyncThunk(
  'toplist',
  (arg, { dispatch }) => {
    getToplistData().then((res) => {
      dispatch(changeToplistAction(res?.list))
    })
  }
)
export const fetchPlaylistDataAction = createAsyncThunk(
  'playlist',
  (id: number, { dispatch }) => {
    getPlaylistData(id).then((res) => {
      dispatch(changePlaylistAction(res?.playlist))
    })
  }
)

interface IRankingState {
  toplist: any
  currentIndex: number
  playlist: any
  currentStatus: string
}
const initialState: IRankingState = {
  toplist: [],
  currentIndex: 0,
  playlist: [],
  currentStatus: ''
}
const rankingSlice = createSlice({
  name: 'ranking',
  initialState,
  reducers: {
    changeToplistAction(state, { payload }) {
      state.toplist = payload
    },
    changeCurrentIndexAction(state, { payload }) {
      state.currentIndex = payload
    },
    changePlaylistAction(state, { payload }) {
      state.playlist = payload
    },
    changeCurrentStatusAction(state, { payload }) {
      state.currentStatus = payload
    }
  }
})
export const {
  changeToplistAction,
  changeCurrentIndexAction,
  changePlaylistAction,
  changeCurrentStatusAction
} = rankingSlice.actions
export default rankingSlice.reducer
