import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import {
  getBannersData,
  getHotRecommendData,
  getNewAlbumData,
  getRankingData,
  getSettledSingerData
} from '@/service/modules/discover/recommend'

export const fetchRecommentDataAction = createAsyncThunk(
  'fetchRecommentData',
  (arg, { dispatch }) => {
    getBannersData().then((res) => {
      dispatch(changeBannersDataAction(res?.banners))
    })
    getHotRecommendData().then((res) => {
      dispatch(changeHotRecommendDataAction(res?.result))
    })
    getNewAlbumData().then((res) => {
      dispatch(changeNewAlbumDataAction(res?.albums))
    })

    getSettledSingerData().then((res) => {
      dispatch(changeSettledSingerDataAction(res?.artists))
    })
  }
)

const rankingIds = [19723756, 3779629, 2884035]
export const fetchRankingDataAction = createAsyncThunk(
  'fetchRankingData',
  (_, { dispatch }) => {
    // 保障一：获取到所有的结果后，进行dispatch操作
    // 保障二：获取到的结果一定有正确的顺序
    const promises: Promise<any>[] = []
    for (const id of rankingIds) {
      promises.push(getRankingData(id))
    }
    Promise.all(promises).then((res) => {
      const playlists = res
        .filter((item) => item.playlist)
        .map((item) => item.playlist)
      dispatch(changeRankingDataAction(playlists))
    })

    // 每个请求单独处理
    // for (const id of rankingIds) {
    //   getRankingData(id).then((res) => {
    //     switch (id) {
    //       case 19723756:
    //         dispatch(changeSoaringDataAction(res.playlist))
    //         break
    //       case 3779629:
    //         dispatch(changeNewSongDataAction(res.playlist))
    //         break
    //       case 2884035:
    //         dispatch(changeOriginalDataAction(res.playlist))
    //         break
    //     }
    //   })
    // }
  }
)
interface IRecommendState {
  banners: any[]
  hotRecommend: any[]
  newAlbums: any[]
  ranking: any[]
  // soaring: any //飙升榜
  // newSong: any //新歌榜
  // original: any //原创榜
  settledSinger: any[]
}
const initialState: IRecommendState = {
  banners: [],
  hotRecommend: [],
  newAlbums: [],
  ranking: [],
  settledSinger: []
}

const recommendSlice = createSlice({
  name: 'recommend',
  initialState,
  reducers: {
    changeBannersDataAction(state, { payload }) {
      state.banners = payload
    },
    changeHotRecommendDataAction(state, { payload }) {
      state.hotRecommend = payload
    },
    changeNewAlbumDataAction(state, { payload }) {
      state.newAlbums = payload
    },
    changeRankingDataAction(state, { payload }) {
      state.ranking = payload
    },
    changeSettledSingerDataAction(state, { payload }) {
      state.settledSinger = payload
    }
  }
})
export const {
  changeBannersDataAction,
  changeHotRecommendDataAction,
  changeNewAlbumDataAction,
  changeRankingDataAction,
  changeSettledSingerDataAction
} = recommendSlice.actions

export default recommendSlice.reducer
