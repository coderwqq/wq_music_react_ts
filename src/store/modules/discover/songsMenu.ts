import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { getSongsMenuData } from '@/service/modules/discover/songsMenu'

export const fetchSongsMenuDataAction = createAsyncThunk(
  'playlist',
  (currentPage: number, { dispatch }) => {
    getSongsMenuData((currentPage - 1) * 35).then((res) => {
      dispatch(changeCategorySongsAction(res))
    })
  }
)

interface ISongsMenuState {
  currentPage: number
  currentCategory: string
  categorySongs: any
  // isLoading: boolean
}
const initialState: ISongsMenuState = {
  currentPage: 1,
  currentCategory: '全部',
  categorySongs: []
  // isLoading: false
}

const playlistSlice = createSlice({
  name: 'songsMenu',
  initialState,
  reducers: {
    changeCurrentPageAction(state, { payload }) {
      state.currentPage = payload
    },
    changeCategorySongsAction(state, { payload }) {
      state.categorySongs = payload
    }
    // changeIsLoadingAction(state, { payload }) {
    //   state.isLoading = payload
    // }
  }
})

export const {
  changeCurrentPageAction,
  changeCategorySongsAction
  // changeIsLoadingAction
} = playlistSlice.actions
export default playlistSlice.reducer
