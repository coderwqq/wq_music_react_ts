import { createSlice } from '@reduxjs/toolkit'

interface ISingerState {
  currentArea: number
  currentType: {
    name: string
    type: number
  }
}
const initialState: ISingerState = {
  currentArea: -1,
  currentType: {
    name: '推荐歌手',
    type: 1
  }
}
const singerSlice = createSlice({
  name: 'singer',
  initialState,
  reducers: {
    changeCurrentAreaAction(state, { payload }) {
      state.currentArea = payload
    },
    changeCurrentTypeAction(state, { payload }) {
      state.currentType.type = payload
    }
  }
})

export const { changeCurrentAreaAction, changeCurrentTypeAction } =
  singerSlice.actions
export default singerSlice.reducer
