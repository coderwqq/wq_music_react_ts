import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import {
  getRadioCategoryData,
  getRecommendProgramData,
  getProgramRankingData,
  getDjCoverData
} from '@/service/modules/discover/djradio'

export const fetchDjradioDataAction = createAsyncThunk(
  'djradio',
  (arg, { dispatch }) => {
    getRadioCategoryData().then((res) => {
      dispatch(changeRadioCategortAction(res.categories))
    })
    getRecommendProgramData().then((res) => {
      dispatch(changeRecommendProgramAction(res.programs))
    })
    getProgramRankingData().then((res) => {
      dispatch(changeProgramRankingAction(res.toplist))
    })

    getDjCoverData().then((res) => {
      dispatch(changeDjCoverAction(res.djRadios))
    })
  }
)

interface IDjradioState {
  radioCategory: any
  recommendProgram: any
  programRanking: any
  djCover: any
}

const initialState: IDjradioState = {
  radioCategory: [],
  recommendProgram: [],
  programRanking: [],
  djCover: []
}

const djradioSlice = createSlice({
  name: 'djradio',
  initialState,
  reducers: {
    changeRadioCategortAction(state, { payload }) {
      state.radioCategory = payload
    },
    changeRecommendProgramAction(state, { payload }) {
      state.recommendProgram = payload
    },
    changeProgramRankingAction(state, { payload }) {
      state.programRanking = payload
    },
    changeDjCoverAction(state, { payload }) {
      state.djCover = payload
    }
  }
})

export const {
  changeRadioCategortAction,
  changeRecommendProgramAction,
  changeProgramRankingAction,
  changeDjCoverAction
} = djradioSlice.actions
export default djradioSlice.reducer
