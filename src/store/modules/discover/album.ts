import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import {
  getHotAlbumData,
  getAllAlbumData
} from '@/service/modules/discover/album'

export const fetchAlbumDataAction = createAsyncThunk(
  'album',
  (arg, { dispatch }) => {
    getHotAlbumData().then((res) => {
      dispatch(changeHotAlbumAction(res.albums))
    })
    getAllAlbumData().then((res) => {
      dispatch(changeAllAlbumAction(res.albums))
    })
  }
)

interface IAlbumState {
  hotAlbum: any
  allAlbum: any
}
const initialState: IAlbumState = {
  hotAlbum: [],
  allAlbum: []
}

const albumSlice = createSlice({
  name: 'album',
  initialState,
  reducers: {
    changeHotAlbumAction(state, { payload }) {
      state.hotAlbum = payload
    },
    changeAllAlbumAction(state, { payload }) {
      state.allAlbum = payload
    }
  }
})

export const { changeHotAlbumAction, changeAllAlbumAction } = albumSlice.actions
export default albumSlice.reducer
