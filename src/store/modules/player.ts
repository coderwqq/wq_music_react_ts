import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import type { IRootState } from '..'
import { getSongDetail, getLyricDetail } from '@/service/modules/player'
import { Ilyric, parseLyric } from '@/utils/parse_lyric'

interface IThunkState {
  state: IRootState
}

export const fetchLyricDetailAction = createAsyncThunk(
  'lyricDetail',
  (id: number, { dispatch }) => {
    getLyricDetail(id).then((res) => {
      const lyricString = res?.lrc?.lyric
      const lyrics = parseLyric(lyricString)
      dispatch(changeLyricsAction(lyrics))
    })
  }
)
export const fetchCurrentSongAction = createAsyncThunk<
  void,
  number,
  IThunkState
>('currentSong', (id, { dispatch, getState }) => {
  const playSongList = getState().playerReducer.playSongList
  const findIndex = playSongList.findIndex((item) => item.id === id)

  // 尝试从列表中获取某首歌
  if (findIndex === -1) {
    // 没能获取情况
    getSongDetail(id).then((res) => {
      if (!res.songs.length) return
      const song = res.songs[0]

      const newPlaySongList = [...playSongList]
      newPlaySongList.push(song)
      dispatch(changeCurrentSongAction(song))
      dispatch(changePlaySongListAction(newPlaySongList))
      dispatch(changePlaySongIndexAction(newPlaySongList.length - 1))
    })
  } else {
    // 能获取情况
    const song = playSongList[findIndex]
    dispatch(changeCurrentSongAction(song))
    dispatch(changePlaySongIndexAction(findIndex))
  }

  dispatch(fetchLyricDetailAction(id))
})
export const toggleSongAction = createAsyncThunk<void, boolean, IThunkState>(
  'toggleSong',
  (isNext, { dispatch, getState }) => {
    const playSongList = getState().playerReducer.playSongList
    const playSongIndex = getState().playerReducer.playSongIndex
    const playMode = getState().playerReducer.playMode

    // 根据不同的模式计算下一首歌曲的索引
    let newSongIndex = playSongIndex
    if (playMode === 1) {
      newSongIndex = Math.floor(Math.random() * playSongList.length)
    } else {
      newSongIndex = isNext ? newSongIndex + 1 : newSongIndex - 1
      if (newSongIndex > playSongList.length - 1) newSongIndex = 0
      if (newSongIndex < 0) newSongIndex = playSongList.length - 1
    }

    const song = playSongList[newSongIndex]
    dispatch(changeCurrentSongAction(song))
    dispatch(changePlaySongIndexAction(newSongIndex))

    dispatch(fetchLyricDetailAction(song.id))
  }
)

interface IPlayerState {
  currentSong: any
  lyrics: Ilyric[]
  lyricIndex: number
  playSongList: any[]
  playSongIndex: number
  playMode: number
}
const initialState: IPlayerState = {
  currentSong: {},
  lyrics: [],
  lyricIndex: -1,
  playSongList: [
    {
      name: '玄鸟（《长月烬明》插曲）',
      id: 2040982214,
      pst: 0,
      t: 0,
      ar: [
        {
          id: 51294945,
          name: '濑了个濑',
          tns: [],
          alias: []
        }
      ],
      alia: [],
      pop: 100,
      st: 0,
      rt: '',
      fee: 0,
      v: 2,
      crbt: null,
      cf: '',
      al: {
        id: 164080181,
        name: '黑月光拿稳be剧本',
        picUrl:
          'https://p2.music.126.net/NY6lWEdJXLCQ87E8VvAVFg==/109951168557724049.jpg',
        tns: [],
        pic_str: '109951168557724049',
        pic: 109951168557724050
      },
      dt: 258023,
      h: {
        br: 320001,
        fid: 0,
        size: 10322925,
        vd: -39575,
        sr: 48000
      },
      m: {
        br: 192001,
        fid: 0,
        size: 6193773,
        vd: -37053,
        sr: 48000
      },
      l: {
        br: 128001,
        fid: 0,
        size: 4129197,
        vd: -35528,
        sr: 48000
      },
      sq: {
        br: 920649,
        fid: 0,
        size: 29693663,
        vd: -39532,
        sr: 48000
      },
      hr: null,
      a: null,
      cd: '01',
      no: 1,
      rtUrl: null,
      ftype: 0,
      rtUrls: [],
      djId: 0,
      copyright: 0,
      s_id: 0,
      mark: 128,
      originCoverType: 2,
      originSongSimpleData: null,
      tagPicList: null,
      resourceState: true,
      version: 2,
      songJumpInfo: null,
      entertainmentTags: null,
      awardTags: null,
      single: 0,
      noCopyrightRcmd: null,
      mst: 9,
      cp: 0,
      rtype: 0,
      rurl: null,
      mv: 0,
      publishTime: 0
    },
    {
      name: '寄长月-《长月烬明》插曲-cover不才',
      id: 2037525680,
      pst: 0,
      t: 0,
      ar: [
        {
          id: 48574331,
          name: '捡破烂的',
          tns: [],
          alias: []
        },
        {
          id: 48203925,
          name: '挽君',
          tns: [],
          alias: []
        }
      ],
      alia: [],
      pop: 100,
      st: 0,
      rt: '',
      fee: 0,
      v: 5,
      crbt: null,
      cf: '',
      al: {
        id: 163314967,
        name: '《长月烬明》翻唱',
        picUrl:
          'https://p1.music.126.net/IvqhxUgDzVx8OXvbq08pkA==/109951168532804095.jpg',
        tns: [],
        pic_str: '109951168532804095',
        pic: 109951168532804100
      },
      dt: 289327,
      h: {
        br: 320000,
        fid: 0,
        size: 11575423,
        vd: -35217,
        sr: 44100
      },
      m: {
        br: 192000,
        fid: 0,
        size: 6945271,
        vd: -32587,
        sr: 44100
      },
      l: {
        br: 128000,
        fid: 0,
        size: 4630195,
        vd: -30872,
        sr: 44100
      },
      sq: {
        br: 808103,
        fid: 0,
        size: 29225777,
        vd: -33827,
        sr: 44100
      },
      hr: null,
      a: null,
      cd: '01',
      no: 1,
      rtUrl: null,
      ftype: 0,
      rtUrls: [],
      djId: 0,
      copyright: 0,
      s_id: 0,
      mark: 128,
      originCoverType: 2,
      originSongSimpleData: null,
      tagPicList: null,
      resourceState: true,
      version: 5,
      songJumpInfo: null,
      entertainmentTags: null,
      awardTags: null,
      single: 0,
      noCopyrightRcmd: null,
      rtype: 0,
      rurl: null,
      mst: 9,
      cp: 0,
      mv: 0,
      publishTime: 0
    }
  ],
  playSongIndex: -1,
  playMode: 0 // 0 顺序播放；1 随意播放；2 单曲循坏
}

const playerSlice = createSlice({
  name: 'player',
  initialState,
  reducers: {
    changeCurrentSongAction(state, { payload }) {
      state.currentSong = payload
    },
    changeLyricsAction(state, { payload }) {
      state.lyrics = payload
    },
    changeLyricIndexAction(state, { payload }) {
      state.lyricIndex = payload
    },
    changePlaySongListAction(state, { payload }) {
      state.playSongList = payload
    },
    changePlaySongIndexAction(state, { payload }) {
      state.playSongIndex = payload
    },
    changePlayModeAction(state, { payload }) {
      state.playMode = payload
    }
  }
})

export const {
  changeCurrentSongAction,
  changeLyricsAction,
  changeLyricIndexAction,
  changePlaySongListAction,
  changePlaySongIndexAction,
  changePlayModeAction
} = playerSlice.actions
export default playerSlice.reducer
