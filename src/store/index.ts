import { configureStore } from '@reduxjs/toolkit'
import { TypedUseSelectorHook, useSelector, useDispatch } from 'react-redux'

import recommendReducer from './modules/discover/recommend'
import rankingReducer from './modules/discover/ranking'
import playerReducer from './modules/player'
import songsMenuReducer from './modules/discover/songsMenu'
import djradioReducer from './modules/discover/djradio'
import singerReducer from './modules/discover/singer'
import albumReducer from './modules/discover/album'

const store = configureStore({
  reducer: {
    recommendReducer,
    rankingReducer,
    playerReducer,
    songsMenuReducer,
    djradioReducer,
    singerReducer,
    albumReducer
  }
})

type GetStateFnType = typeof store.getState
export type IRootState = ReturnType<GetStateFnType>
type GetDispatchType = typeof store.dispatch

export const useAppSelector: TypedUseSelectorHook<IRootState> = useSelector
export const useAppDispatch: () => GetDispatchType = useDispatch

export default store
