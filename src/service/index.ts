import { BASE_URL, TIME_OUT } from './config'
import WQRequest from './request'

const wqRequest = new WQRequest({
  baseURL: BASE_URL,
  timeout: TIME_OUT,
  interceptors: {
    requestInterceptor: (config) => {
      console.log('请求拦截成功')
      return config
    },
    requestInterceptorCatch: (err) => {
      console.log('请求拦截失败')
      return err
    },
    responseInterceptor: (res) => {
      console.log('请求响应成功')
      return res
    },
    responseInterceptorCatch: (err) => {
      console.log('请求响应失败')
      return err
    }
  }
})

export default wqRequest
