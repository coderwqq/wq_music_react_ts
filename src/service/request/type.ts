import type { AxiosRequestConfig, AxiosResponse } from 'axios'

export interface WQRequestInterceptor<T = AxiosResponse> {
  requestInterceptor?: (config: AxiosRequestConfig) => AxiosRequestConfig
  requestInterceptorCatch?: (err: any) => any
  responseInterceptor?: (res: T) => T
  responseInterceptorCatch?: (err: any) => any
}

export interface WQRequestConfig<T = AxiosResponse> extends AxiosRequestConfig {
  interceptors?: WQRequestInterceptor<T>
}
