import axios from 'axios'
import type { AxiosInstance } from 'axios'
import type { WQRequestConfig, WQRequestInterceptor } from './type'

class WQRequest {
  instance: AxiosInstance
  interceptors?: WQRequestInterceptor

  constructor(config: WQRequestConfig) {
    this.instance = axios.create(config)

    this.instance.interceptors.request.use(
      (config) => {
        // console.log('请求成功')
        return config
      },
      (err) => {
        return err
      }
    )
    this.instance.interceptors.response.use(
      (res) => {
        // console.log('请求响应成功')
        return res.data
      },
      (err) => {
        return err
      }
    )

    // this.instance.interceptors.request.use(
    //   this.interceptors?.requestInterceptor,
    //   this.interceptors?.requestInterceptorCatch
    // )
    // this.instance.interceptors.response.use(
    //   this.interceptors?.responseInterceptor,
    //   this.interceptors?.responseInterceptorCatch
    // )
  }

  request<T = any>(config: WQRequestConfig<T>) {
    // 单次请求的成功拦截处理
    if (this.interceptors?.requestInterceptor) {
      config = this.interceptors.requestInterceptor(config)
    }

    // 返回Promise
    return new Promise<T>((resolve, reject) => {
      this.instance
        .request<any, T>(config)
        .then((res) => {
          // 单词响应的成功拦截处理
          if (config.interceptors?.responseInterceptor) {
            res = config.interceptors.responseInterceptor(res)
          }
          resolve(res)
        })
        .catch((err) => {
          reject(err)
        })
    })
  }

  get<T = any>(config: WQRequestConfig<T>) {
    return this.request({ ...config, method: 'GET' })
  }
  post<T = any>(config: WQRequestConfig<T>) {
    return this.request({ ...config, method: 'POST' })
  }
  delete<T = any>(config: WQRequestConfig<T>) {
    return this.request({ ...config, method: 'DELETE' })
  }
  patch<T = any>(config: WQRequestConfig<T>) {
    return this.request({ ...config, method: 'PATCH' })
  }
}

export default WQRequest
