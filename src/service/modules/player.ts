import wqRequest from '..'

export function getSongDetail(ids: number) {
  return wqRequest.get({
    url: '/song/detail',
    params: {
      ids
    }
  })
}

export function getLyricDetail(id: number) {
  return wqRequest.get({
    url: '/lyric',
    params: {
      id
    }
  })
}
