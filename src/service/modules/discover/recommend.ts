import wqRequest from '@/service'

export function getBannersData() {
  return wqRequest.get({
    url: '/banner'
  })
}

export function getHotRecommendData(limit = 8) {
  return wqRequest.get({
    url: '/personalized',
    params: {
      limit
    }
  })
}

export function getNewAlbumData() {
  return wqRequest.get({
    url: '/album/newest'
  })
}

export function getRankingData(id: number) {
  return wqRequest.get({
    url: '/playlist/detail',
    params: {
      id
    }
  })
}

export function getSettledSingerData(limit = 5) {
  return wqRequest.get({
    url: '/artist/list',
    params: {
      limit
    }
  })
}
