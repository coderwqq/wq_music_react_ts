import wqRequest from '@/service'

export function getToplistData() {
  return wqRequest.get({
    url: '/toplist'
  })
}

export function getPlaylistData(id: number) {
  return wqRequest.get({
    url: '/playlist/detail',
    params: {
      id
    }
  })
}
