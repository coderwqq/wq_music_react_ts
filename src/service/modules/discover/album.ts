import wqRequest from '@/service'

export function getHotAlbumData() {
  return wqRequest.get({
    url: '/album/newest'
  })
}

export function getAllAlbumData(area = 'ALL', limit = '30') {
  return wqRequest.get({
    url: '/album/new',
    params: {
      area,
      limit
    }
  })
}
