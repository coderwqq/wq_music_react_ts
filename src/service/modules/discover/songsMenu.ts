import wqRequest from '@/service'

export function getSongsMenuData(
  offset = 0,
  cat = '全部',
  order = 'hot',
  limit = 35
) {
  return wqRequest.get({
    url: '/top/playlist/',
    params: {
      order,
      cat,
      limit,
      offset
    }
  })
}
