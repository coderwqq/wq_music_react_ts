import wqRequest from '@/service'

export function getRadioCategoryData() {
  return wqRequest.get({
    url: '/dj/catelist'
  })
}

export function getRecommendProgramData() {
  return wqRequest.get({
    url: '/program/recommend'
  })
}

export function getProgramRankingData(limit = 10) {
  return wqRequest.get({
    url: '/dj/program/toplist',
    params: {
      limit
    }
  })
}

export function getDjCoverData(type = 2001) {
  return wqRequest.get({
    url: '/dj/recommend/type',
    params: {
      type
    }
  })
}
