import React, { Suspense, useEffect } from 'react'
import { useRoutes } from 'react-router-dom'

import { useAppDispatch } from './store'
import { fetchCurrentSongAction } from './store/modules/player'
import routes from './router'
import AppHeader from '@/components/app_header'
import AppFooter from '@/components/app_footer'
import Player from './views/player'

function App() {
  const dispatch = useAppDispatch()
  useEffect(() => {
    dispatch(fetchCurrentSongAction(2042841771))
  }, [])

  return (
    <div className="App">
      <AppHeader />
      <Suspense fallback="">{useRoutes(routes)}</Suspense>
      <AppFooter />
      <Player />
    </div>
  )
}

export default App
